<?php
/*
 * This file is part of PHP CS Fixer.
 * (c) kcloze <pei.greet@qq.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

define('SWOOLE_JOBS_ROOT_PATH', __DIR__);
//ini_set('default_socket_timeout', -1);
date_default_timezone_set('Asia/Shanghai');
//echo SWOOLE_JOBS_ROOT_PATH . '/vendor/autoload.php';exit;

//require SWOOLE_JOBS_ROOT_PATH . '/vendor/autoload.php';                                                                                  
require(__DIR__ . '/vendor/autoload.php');
require __DIR__ .'/vendor/yiisoft/yii2/Yii.php'; 
require(__DIR__ . '/common/config/bootstrap.php');                                                                                       
require(__DIR__ . '/console/config/bootstrap.php');                                                                                      
                                                                                                                                         
$fconfig = yii\helpers\ArrayHelper::merge(                                                                                                
    require(__DIR__ . '/common/config/main.php'),                                                                                        
    require(__DIR__ . '/common/config/main-local.php'),                                                                                  
    require(__DIR__ . '/console/config/main.php')
); 

$config = require(__DIR__ . '/common/config/swoole-jobs.php');
$config['fconfig'] = $fconfig;
//require SWOOLE_JOBS_ROOT_PATH . '/vendor/autoload.php';
//require APP_PATH . '/vendor/yiisoft/yii2/Yii.php';
//$config = require_once APP_PATH . '/config/swoole-jobs.php';
//$config = require_once SWOOLE_JOBS_ROOT_PATH . '/config.php';
$console = new Kcloze\Jobs\Console($config);
$console->run();
