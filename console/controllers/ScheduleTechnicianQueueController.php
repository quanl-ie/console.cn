<?php
namespace console\controllers;

use common\models\Common;
use common\models\ScheduleTechnician;
use console\models\ScheduleTechnicianQueue;
use console\models\Work;
use console\models\WorkProcess;
use console\models\WorkRelTechnician;
use Yii;
use yii\console\Controller;

class ScheduleTechnicianQueueController extends Controller
{

    /**
     * 处理队列
     * @author xi
     */
	public function  actionStart()
	{
	    $cacheKey = "scheduleTechnicianCache";
	    if(!Yii::$app->cache->exists($cacheKey))
        {
            Yii::$app->cache->set($cacheKey,1,3600);

            $data = ScheduleTechnicianQueue::getAllData();
            if($data)
            {
                foreach ($data as $val)
                {
                    $workNo = $val['work_no'];
                    $technicianArr = WorkRelTechnician::getTechnicianIds($workNo,1);

                    //指派
                    if($val['type'] == 1){
                        $this->assign($workNo,$technicianArr,$val['plan_time']);
                    }
                    //改派
                    else if($val['type'] == 2)
                    {
                        $this->reAssign($workNo,$technicianArr,$val['params'], $val['process_date'],$val['plan_time']);
                    }
                    //开始服务
                    else if($val['type'] == 3)
                    {
                        //检查预约服务时间是否与开始服务时间一样，不一样把预约服务时间的排班数去掉，给开始服务时间排班加上
                        if($val['plan_time'] != $val['process_date'] ){
                            $this->inServices($workNo,$technicianArr,$val['plan_time'],$val['process_date']);
                        }
                    }
                    //完成
                    else if($val['type'] == 4){
                        $this->finsh($workNo, $technicianArr, $val['plan_time'],$val['process_date']);
                    }
                    //取消
                    else if($val['type'] == 5) {
                        $this->cancel($workNo,$technicianArr,$val['plan_time']);
                    }
                    //修改服务时间
                    else if($val['type'] == 6) {
                        $this->changePlantime($workNo,$technicianArr,$val['plan_time'],$val['process_date']);
                    }

                    //执行完删除
                    ScheduleTechnicianQueue::updateAll(['status'=>0], ['id'=>$val['id']]);
                }
            }

            Yii::$app->cache->delete($cacheKey);
        }
	}

    /**
     * 指派
     * @param $technicianArr
     * @param $plantime
     * @param $processDate
     */
    private function assign($workNo,$technicianArr,$plantime)
    {
        if($technicianArr  && $plantime)
        {
            foreach ($technicianArr as $val)
            {
                $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '".$plantime."' ";
                $model = ScheduleTechnician::find()->where($where)->one();
                if($model)
                {
                    if($this->inArrayWorkNo($model->work_nos, $workNo)){
                        continue;
                    }

                    $model->all_order += 1;
                    $model->work_nos   = $this->addWorkNo($model->work_nos, $workNo);
                    $model->save();
                }
                else
                {
                    $departmentArr = Common::getDepartmentIdByTechnicianId($val['technician_id']);

                    $model = new ScheduleTechnician();
                    $model->department_top_id = $departmentArr['department_top_id'];
                    $model->department_id     = $departmentArr['department_id'];
                    $model->tech_id           = $val['technician_id'];
                    $model->schedule_set_id   = '';
                    $model->apply_date        = $plantime;
                    $model->finish_order      = 0;
                    $model->all_order         = 1;
                    $model->description       = '';
                    $model->create_user       = 0;
                    $model->create_time       = date('Y-m-d H:i;s');
                    $model->work_nos          = $workNo;
                    $model->save();
                }
            }
        }
    }

    /**
     * 改派
     * @param $technicianArr
     * @param $plantime
     */
    private function reAssign($workNo,$techArr,$paramsStr,$plantime,$oldPlantime)
    {
        if($paramsStr && $plantime && $oldPlantime)
        {
            $technicianArr = json_decode($paramsStr,true);

            //如果没有改服务时间
            if($plantime == $oldPlantime)
            {
                if(isset($technicianArr['newAddJsIds']))
                {
                    foreach ($technicianArr['newAddJsIds'] as $val)
                    {
                        $where = "tech_id = " . $val . " and date(apply_date) = '".$plantime."' ";
                        $model = ScheduleTechnician::find()->where($where)->one();
                        if($model)
                        {
                            if($this->inArrayWorkNo($model->work_nos, $workNo) == true){
                                continue;
                            }

                            $model->all_order += 1;
                            $model->work_nos   = $this->addWorkNo($model->work_nos, $workNo);
                            $model->save();
                        }
                        else
                        {
                            $departmentArr = Common::getDepartmentIdByTechnicianId($val);

                            $model = new ScheduleTechnician();
                            $model->department_top_id = $departmentArr['department_top_id'];
                            $model->department_id     = $departmentArr['department_id'];
                            $model->tech_id           = $val;
                            $model->schedule_set_id   = '';
                            $model->apply_date        = $plantime;
                            $model->finish_order      = 0;
                            $model->all_order         = 1;
                            $model->description       = '';
                            $model->create_user       = 0;
                            $model->create_time       = date('Y-m-d H:i;s');
                            $model->work_nos          = $workNo;
                            $model->save();
                        }
                    }
                }

                if(isset($technicianArr['delJsIds']))
                {
                    foreach ($technicianArr['delJsIds'] as $val)
                    {
                        $where = "tech_id = " . $val . " and  FIND_IN_SET( ".$workNo.", work_nos)";
                        $models = ScheduleTechnician::find()->where($where)->all();
                        if ($models)
                        {
                            foreach ($models as $model)
                            {
                                if ($model->all_order > 0) {
                                    $model->all_order -= 1;
                                }

                                $model->work_nos = $this->removeWorkNo($model->work_nos, $workNo);
                                $model->save();
                            }
                        }
                    }
                }
            }
            //修改了服务时间
            else
            {
                $allTechnicianIds = array_column($techArr,'technician_id');
                $jsIds = array_diff($allTechnicianIds, $technicianArr['newAddJsIds']);

                $newAddjsIds = $jsIds ;

                if(isset($technicianArr['newAddJsIds'])){
                    $newAddjsIds = array_merge($newAddjsIds,$technicianArr['newAddJsIds']);
                }

                $newAddjsIds = array_unique($newAddjsIds);
                if($newAddjsIds)
                {
                    foreach ($newAddjsIds as $val)
                    {
                        $where = "tech_id = " . $val . " and date(apply_date) = '".$plantime."' ";
                        $model = ScheduleTechnician::find()->where($where)->one();
                        if($model)
                        {
                            if($this->inArrayWorkNo($model->work_nos, $workNo) == true){
                                continue;
                            }

                            $model->all_order += 1;
                            $model->work_nos   = $this->addWorkNo($model->work_nos, $workNo);
                            $model->save();
                        }
                        else
                        {
                            $departmentArr = Common::getDepartmentIdByTechnicianId($val);

                            $model = new ScheduleTechnician();
                            $model->department_top_id = $departmentArr['department_top_id'];
                            $model->department_id     = $departmentArr['department_id'];
                            $model->tech_id           = $val;
                            $model->schedule_set_id   = '';
                            $model->apply_date        = $plantime;
                            $model->finish_order      = 0;
                            $model->all_order         = 1;
                            $model->description       = '';
                            $model->create_user       = 0;
                            $model->create_time       = date('Y-m-d H:i;s');
                            $model->work_nos          = $workNo;
                            $model->save();
                        }
                    }
                }

                $delJsIds = $jsIds;
                if(isset($technicianArr['delJsIds'])){
                    $delJsIds = array_merge($delJsIds, $technicianArr['delJsIds']);
                }

                if($delJsIds)
                {
                    foreach ($delJsIds as $val)
                    {
                        $where = "tech_id = " . $val . " FIND_IN_SET( ".$workNo.", work_nos)";
                        $models = ScheduleTechnician::find()->where($where)->all();
                        if($models)
                        {
                            foreach ($models as $model)
                            {
                                if($model->all_order >0 ){
                                    $model->all_order -= 1;
                                }

                                $model->work_nos = $this->removeWorkNo($model->work_nos, $workNo);
                                $model->save();
                            }
                        }
                    }
                }

            }
        }
    }

    /**
     * 服务中工单修正
     * @param $technicianArr
     * @param $plantime
     * @param $processDate
     */
	private function inServices($workNo,$technicianArr,$plantime,$processDate)
    {
        if($technicianArr && $plantime && $processDate)
        {
            foreach ($technicianArr as $val)
            {
                $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '$plantime' ";
                $model = ScheduleTechnician::find()->where($where)->one();
                if($model)
                {
                    if($model->all_order >0 ){
                        $model->all_order    -= 1;
                    }
                    $model->work_nos = $this->removeWorkNo($model->work_nos, $workNo);
                    $model->save();
                }

                $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '".$processDate."' ";
                $model = ScheduleTechnician::find()->where($where)->one();
                if($model)
                {
                    $model->all_order    += 1;
                    $model->work_nos = $this->addWorkNo($model->work_nos, $workNo);
                    $model->save();
                }
                else
                {
                    $departmentArr = Common::getDepartmentIdByTechnicianId($val['technician_id']);

                    $model = new ScheduleTechnician();
                    $model->department_top_id = $departmentArr['department_top_id'];
                    $model->department_id     = $departmentArr['department_id'];
                    $model->tech_id           = $val['technician_id'];
                    $model->schedule_set_id   = '';
                    $model->apply_date        = $processDate;
                    $model->finish_order      = 0;
                    $model->all_order         = 1;
                    $model->description       = '';
                    $model->create_user       = 0;
                    $model->create_time       = date('Y-m-d H:i;s');
                    $model->work_nos          = $workNo;
                    $model->save();
                }
            }
        }
    }

    /**
     * 完成工单修正
     * @param $technicianArr
     * @param $plantime
     * @param $processDate
     */
    private function finsh($workNo,$technicianArr,$plantime,$processDate)
    {
        if($technicianArr && $plantime && $processDate)
        {
            //查出开始服务时间
            $startServiceTime = '';
            $where = [
                'work_no' => $workNo,
                'type'    => 3
            ];
            $queueArr = ScheduleTechnicianQueue::find()->where($where)->select('process_date')->asArray()->one();
            if($queueArr){
                $startServiceTime = $queueArr['process_date'];
            }

            if($startServiceTime)
            {
                if($startServiceTime != $plantime){
                    $plantime = $startServiceTime;
                }

                if($plantime != $processDate)
                {
                    foreach ($technicianArr as $val)
                    {
                        $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '".$processDate."' ";
                        $model = ScheduleTechnician::find()->where($where)->one();
                        if($model)
                        {
                            $model->finish_order += 1;
                            $model->work_nos      = $this->addWorkNo($model->work_nos, $workNo);
                            $model->save();
                        }
                    }

                    foreach ($technicianArr as $val)
                    {
                        $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '".$plantime."' ";
                        $model = ScheduleTechnician::find()->where($where)->one();
                        if($model)
                        {
                            $model->all_order  -= 1;
                            $model->work_nos   = $this->removeWorkNo($model->work_nos, $workNo);
                            $model->save();
                        }
                    }

                }
                else
                {
                    foreach ($technicianArr as $val)
                    {
                        $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '".$plantime."' ";
                        $model = ScheduleTechnician::find()->where($where)->one();
                        if($model)
                        {
                            $model->finish_order += 1;
                            $model->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * 取消工单
     * @param $technicianArr
     * @param $plantime
     */
    private function cancel($workNo,$technicianArr,$plantime)
    {
        if($technicianArr && $plantime)
        {
            foreach ($technicianArr as $val)
            {
                $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '$plantime' ";
                $model = ScheduleTechnician::find()->where($where)->one();
                if($model)
                {
                    if($model->all_order >0 ){
                        $model->all_order -= 1;
                    }

                    $model->work_nos   = $this->removeWorkNo($model->work_nos, $workNo);
                    $model->save();
                }
            }
        }
    }

    /**
     * 修改工单服务时间
     * @param $technicianArr
     * @param $plantime
     * @author xi
     */
    public function changePlantime($workNo,$technicianArr,$plantime,$processDate)
    {
        if($workNo && $technicianArr && $plantime && $processDate)
        {
            //如果修改的服务时间与以前的时间不一样
            if($plantime != $processDate)
            {
                foreach ($technicianArr as $val)
                {
                    $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '$plantime' ";
                    $model = ScheduleTechnician::find()->where($where)->one();
                    if($model)
                    {
                        if($model->all_order >0 ){
                            $model->all_order -= 1;
                        }

                        $model->work_nos   = $this->removeWorkNo($model->work_nos, $workNo);
                        $model->save();
                    }

                    $where = "tech_id = " . $val['technician_id'] . " and date(apply_date) = '$processDate' ";
                    $model = ScheduleTechnician::find()->where($where)->one();
                    if($model)
                    {
                        $model->all_order += 1;
                        $model->work_nos   = $this->addWorkNo($model->work_nos, $workNo);
                        $model->save();
                    }
                    else
                    {
                        $departmentArr = Common::getDepartmentIdByTechnicianId($val['technician_id']);

                        $model = new ScheduleTechnician();
                        $model->department_top_id = $departmentArr['department_top_id'];
                        $model->department_id     = $departmentArr['department_id'];
                        $model->tech_id           = $val['technician_id'];
                        $model->schedule_set_id   = '';
                        $model->apply_date        = $processDate;
                        $model->finish_order      = 0;
                        $model->all_order         = 1;
                        $model->description       = '';
                        $model->create_user       = 0;
                        $model->create_time       = date('Y-m-d H:i;s');
                        $model->work_nos          = $workNo;
                        $model->save();
                    }
                }
            }
        }
    }

    /**
     * 初始化技师排班数据
     * @author xi
     */
	public function actionInitData()
    {
        $page = 1;
        do
        {
            $workData = Work::getAllWorkData($page);
            if(!$workData){
                break;
            }
            $page++;

            $workNos = array_column($workData,'work_no');

            $workRelArr = WorkRelTechnician::getAllDataByWorkNos($workNos);

            foreach ($workData as $val)
            {
                $finishOrder = 0;

                $processTime = '';
                if($val['status'] == 2){
                    $processTime = date('Y-m-d',$val['plan_time']);
                }
                else if(in_array($val['status'],[3,5]))
                {
                    $processTime = WorkProcess::getProcessTimeByType($val['work_no'],4);
                    if($processTime){
                        $processTime = date('Y-m-d',$processTime);
                    }
                    else {
                        $processTime = '';
                    }
                }
                else if($val['status'] == 4)
                {
                    $processTime = WorkProcess::getProcessTimeByType($val['work_no'],5);
                    if($processTime){
                        $processTime = date('Y-m-d',$processTime);
                    }
                    else {
                        $processTime = '';
                    }

                    $finishOrder = 1;
                }

                if(isset($workRelArr[$val['work_no']]) && $processTime)
                {
                    foreach ($workRelArr[$val['work_no']] as $workVal)
                    {
                        $where = "tech_id = " . $workVal['technician_id'] . " and date(apply_date) = '$processTime' ";
                        $model = ScheduleTechnician::find()->where($where)->one();
                        if($model)
                        {

                            $model->finish_order += $finishOrder;
                            $model->all_order    += 1;
                            $model->work_nos      = $this->addWorkNo($model->work_nos,$val['work_no']);
                            $model->save();
                        }
                        else
                        {
                            $departmentArr = Common::getDepartmentIdByTechnicianId($workVal['technician_id']);

                            $model = new ScheduleTechnician();
                            $model->department_top_id = $departmentArr['department_top_id'];
                            $model->department_id     = $departmentArr['department_id'];
                            $model->tech_id           = $workVal['technician_id'];
                            $model->schedule_set_id   = '';
                            $model->apply_date        = $processTime;
                            $model->finish_order      = $finishOrder;
                            $model->all_order         = 1;
                            $model->description       = '';
                            $model->create_user       = 0;
                            $model->create_time       = date('Y-m-d H:i;s');
                            $model->work_nos          = $val['work_no'];
                            $model->save();
                        }
                    }
                }
            }

        }
        while(true);

    }

    private function addWorkNo($workNosStr,$workNo)
    {
        $workNos = [];
        if($workNo)
        {
            $workNos = explode(',',$workNosStr);
            if(!in_array($workNo,$workNos)){
                array_push($workNos,$workNo);
            }
        }

        return implode(',', array_filter($workNos));
    }

    private function removeWorkNo($workNosStr, $workNo)
    {
        $workNos = [];
        if($workNo)
        {
            $workNos = explode(',',$workNosStr);
            if(array_search($workNo, $workNos) !== false){
                unset( $workNos[array_search($workNo,$workNos)] );
            }
        }

        return implode(',',$workNos);
    }

    private function inArrayWorkNo($workNosStr, $workNo)
    {
        if($workNosStr && $workNo)
        {
            $workNos = explode(',',$workNosStr);
            return in_array($workNo,$workNos);
        }

        return false;
    }
}


