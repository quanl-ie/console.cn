<?php
namespace console\controllers;

use console\models\CouponPushLogs;
use console\models\punchCard\PunchCardPush;
use Yii;
use yii\console\Controller;
use console\models\PushObject;
use console\models\PushGroup;
use common\helpers\Helper;
use console\models\Focus;
use console\models\CourseAuthorization;
use JPush\Model as M;
use JPush\JPushClient;
use JPush\Exception\APIConnectionException;
use JPush\Exception\APIRequestException;
use console\models\AssignmentCategory;
use console\models\PushYqk;
use console\models\Message;
use console\models\Technician;

class JPushController extends Controller
{
    //最大用几个进程发送
    const MAX_PAGES   = 2;

    const IOS         = 1;
	const IOS_APP_KEY = 'f2fbde99a34bc23ebabf2312';
	const IOS_SECRET  = 'e336300890ae9afc5c86466a';
	const ANDROID_APP_KEY = 'f2fbde99a34bc23ebabf2312';
	const ANDROID_SECRET  = 'e336300890ae9afc5c86466a';
	const LOCK_KEY = 'redis_push_key';

	const TITLE = '智能云集';
	const XM_MAX_PAGES = 20;

	//线上 true 测试 false
	const ONLINE = false;

    private static $_ios_instance = null;
    private static $_android_instance = null;

    public static function getIosInstance()
	{
        if (is_null ( self::$_ios_instance ))
		{
			self::$_ios_instance = new JPushClient(self::IOS_APP_KEY, self::IOS_SECRET);
        }
        return self::$_ios_instance;
    }

    public static function getAndroidInstance()
    {
        if (is_null ( self::$_android_instance ))
        {
            self::$_android_instance = new JPushClient(self::ANDROID_APP_KEY, self::ANDROID_SECRET);
        }
        return self::$_android_instance;
    }
    
    /**
     * 单条消息
     * @param int userId
     * @param string $notification 发送内容
     * @param string $objectType 发送类型
     * @param string $objectId  到达的id
     * @return array
     */
    public function actionPushMsg() {
        //接受要发送的信息
        $title        = Yii::$app->request->post('title','');//发送标题
        $content      = Yii::$app->request->post('content','');//发送内容
        $tec_id       = Yii::$app->request->post('technician_id',0);//用户ID
        $type         = Yii::$app->request->post('type',0);//消息类型  1、订单消息  2、技师审核消息  3、异地登陆通知
        $type_content = Yii::$app->request->post('type_content','');//消息跳转条件  type为1该字段为订单号   type为2该字段为技师ID   
        $jpush_id     = Yii::$app->request->post('jpush_id','');//消息跳转条件  type为1该字段为订单号   type为2该字段为技师ID   
        $status = true;
        $msg    = '';
        
        //查询用户的极光唯一ID和设备
        $tecArr = Technician::find()->where(['id' => $tec_id])->asArray()->one();
        //生成要发送的内容
        $userId     = $tecArr['jpush_id'];
        $objectType = $type;
        $objectId   = $type_content;
        $platform   = $tecArr['mobile_system'];
        $alert  = [
                    "title" => $title,
                    "alert" => $content
                ];
        if($platform == 'android') {
            $alert = [
                'android' => [
                    "title" => $title,
                    "alert" => $content
                ]
            ];
        }
        if($type == 3) {
            $userId = $jpush_id;
        }
        //调用发送接口
        $res = $this->sendSingle($userId, json_encode($alert), $objectType, $objectId, $platform);
        if($res['status'] != 1) {
            $status = false;
            $msg = $res['message'];
        }
        return json_encode(['success' => $status, 'message' => $msg]);
    }
    

    /**
     * 测试发送
     * @param int userId
     * @param string $notification 发送内容
     * @param string $objectType 发送类型
     * @param string/int $objectId  到达的id
     * @return array
     * @author xi
     * @date 2017-07-15
     */
    public function actionTest($pushId,$title,$subTitle)
    {
        $android = [
            "title" => $title,
            "alert"  => $subTitle
        ];
		
		$ios = [
            "alert"  => $title
        ];

        print_r($this->sendSingle($pushId,json_encode($ios),'1','201710230c52144295','ios'));
        print_r($this->sendSingle($pushId, json_encode($android),'1','201710230c52144295','android'));
    }


	/**
	 * 发送单个
	 * @param array $userIds 用户ids
	 * @param string $notification 发送内容
	 * @param string $objectType 发送类型
	 * @param string/int $objectId  到达的id
	 * @return array
	 * @author xi
	 * @date 2017-07-15
	 */
	private function sendSingle($userId,$notification,$objectType,$objectId,$platform)
    {
        $data['userid'] = $userId;
        $data['notification'] = $notification;
        $data['objectType'] = $objectType;
        $data['objectId'] = $objectId;
        $data['platform'] = $platform;
		$status  = 0;
		$data    = [];
		$message = '';
		$paramsJson = '';

		if(!$userId){
			$message = 'userIds 不能为空';
		}
		else if(trim($notification) == ''){
			$message = 'notification 不能为空';
		}
		else if(!in_array($objectType,[1,2,3,4,5,6,7])){
			$message = "objectType 取值范围为：[1,2,3,4,5,6,7]";
		}
		else if(!$objectId){
			$message = "objectId 不能为空";
		}
		else if(!in_array($platform, ['ios','android'])){
		    $message = "platform 取值范围为：['ios','android']";
		}
		else
		{
		    $alias = [$userId];

			$extend = array(
				'object_type' => $objectType,
				'object_id'   => $objectId,
			);

			$data = [
			    'msg_id' => ''
			];

			try
			{
			    $notificationParams = '';
			    if($platform == 'ios'){
				    $client = self::getIosInstance();
				    $notificationParams = M\ios($notification, NULL, null, null, $extend, null);
			    }
			    else if($platform == 'android'){
			        $client = self::getAndroidInstance();
			        $notificationParams = M\android($notification, NULL, null, $extend);
			    }
				
				$result = $client->push();
				$result->setPlatform(M\Platform($platform));
				$result->setAudience(M\Audience(M\registration_id($alias)));
				$result->setNotification(M\notification($notification, $notificationParams));

                if($platform == 'android') {
					$notification = [
						"android" => array_merge( [
							"builder_id" =>  1,
							"extras" =>$extend
						],json_decode($notification,true))
					];
					
                    $result->setNotification($notification);
                }
				else 
				{
					$notification = [
						"ios" => array_merge( [
							 "sound" => "default",
							 "badge" => "+1",
							"extras" =>$extend
						],json_decode($notification,true))
					];
					
                    $result->setNotification($notification);
				}
				$result->setOptions(M\options(null, 10000, null, self::ONLINE));
				$paramsJson = $result->getJSON();
				$response = $result->send();
                //判断接口频率是否用完，如果快要没了，就等一下
                if($response->response->headers['x-rate-limit-remaining'] <= 5)
                {
                    //还有多少秒归零 
                    $limtReset = $response->response->headers['x-rate-limit-reset'];
                    sleep($limtReset);
                }

				$status = 1;
				$data = [
					'msg_id' => (string)$response->msg_id,
				];
				$message = $response->json;
			}
			catch (APIRequestException $e)
			{
				$message = json_encode([
					'httpCode' => $e->httpCode,
					'code'     => $e->code,
					'message'  => $e->message,
					'json'     => $e->json,
					'rateLimitLimit'     => $e->rateLimitLimit,
					'rateLimitRemaining' => $e->rateLimitRemaining,
					'rateLimitReset'     => $e->rateLimitReset
				]);

				$jsonArr = json_decode($e->json,true);
				if(isset($jsonArr['msg_id'])){
    				$data = [
    				    'msg_id' => number_format($jsonArr['msg_id'],0,'','')
    				];
				}

				//判断接口频率是否用完，如果快要没了，就等一下
				if($e->rateLimitRemaining <= 5)
				{
				    //还有多少秒归零
				    sleep($e->rateLimitReset);
				}
			}
			catch (APIConnectionException $e)
			{
				$message = json_encode([
					'message'           => $e->getMessage(),
					'isResponseTimeout' => $e->isResponseTimeout
				]);
			}
		}

       return [
			'status'     => $status,
			'data'       => $data,
			'message'    => $message,
			'paramsJson' => $paramsJson
	   ];
    }

	/**
	 * 发送全部
	 * @param string $notification 发送内容
	 * @param string $objectType 发送类型
	 * @param string/int $objectId  到达的id
	 * @param string $platform
	 * @return array
	 * @author xi
	 * @date 2017-07-15
	 */
	private function sendAll($notification,$objectType,$objectId,$platform)
    {
		$status  = 0;
		$data    = [];
		$message = '';
		$paramsJson = '';

		if(trim($notification) == ''){
			$message = 'notification 不能为空';
		}
		else if(!in_array($objectType,['goods','assignment','subject','webview',''])){
			$message = "objectType 取值范围为：['goods','assignment','subject','webview']";
		}
		else if(!$objectId){
			$message = "objectId 不能为空";
		}
		else if(!in_array($platform, ['ios','android'])){
		    $message = "platform 取值范围为：['ios','android']";
		}
		else
		{
			$extend = array(
				'object_type' => $objectType,
				'object_id'   => $objectId,
			);

			$data = [
			    'msg_id' => ''
			];

			try
			{
			    $notificationParams = '';
			    if($platform == 'ios'){
				    $client = self::getIosInstance();
				    $notificationParams = M\ios($notification, NULL, null, null, $extend, null);
			    }
			    else if($platform == 'android'){
			        $client = self::getAndroidInstance();
			        //$notificationParams = M\android($notification, NULL, null, $extend);
			    }
				$result = $client->push();
				$result->setPlatform(M\Platform($platform));
				$result->setAudience('all');

                if($platform == 'android') {
                    $result->setNotification(json_decode($notification,true));
                }
                else{
                    $result->setNotification(M\notification($notification, $notificationParams));
                }

				$result->setOptions(M\options(null, 10000, null, null));

				$paramsJson = $result->getJSON();
				$response = $result->send();

				$status = 1;
				$data = [
					'msg_id' => $response->msg_id,
				];
				$message = $response->json;
			}
			catch (APIRequestException $e)
			{
				$message = json_encode([
					'httpCode' => $e->httpCode,
					'code'     => $e->code,
					'message'  => $e->message,
					'json'     => $e->json,
					'rateLimitLimit'     => $e->rateLimitLimit,
					'rateLimitRemaining' => $e->rateLimitRemaining,
					'rateLimitReset'     => $e->rateLimitReset
				]);

				print_r($message);die;

				$jsonArr = json_decode($e->json);
				$data = [
				    'msg_id' => $jsonArr->msg_id
				];
			}
			catch (APIConnectionException $e)
			{
				$message = json_encode([
					'message'           => $e->getMessage(),
					'isResponseTimeout' => $e->isResponseTimeout
				]);
			}
		}


       $result =  [
			'status'     => $status,
			'data'       => $data,
			'message'    => $message,
			'paramsJson' => $paramsJson
	   ];
       //$this->logs(" platform:" . $platform. " " . json_encode($result) );
       return $result;
    }

    /**
     * 获取发送结果
     * @param string $msgIds msg_ids
     * @return array
     * @author xi
     * @date 2017-07-15
     */
    private function getReport($msgIds)
    {
        $status  = 0;
        $message = '';
        $data    = [];

        try
        {
            $client  = self::getInstance();
            $result = $client->report($msgIds);

            foreach($result->received_list as  $item)
            {
                $data [$item->msg_id] = [
                    'android_received' => $item->android_received ,
                    'ios_apns_sent'    => $item->ios_apns_sent
                ];
            }
            $status = 1;

            //判断接口频率是否用完，如果快要没了，就等一下
            if($result->response->headers['x-rate-limit-remaining'] == 1)
            {
                //还有多少秒归零 
                $limtReset = $result->response->headers['x-rate-limit-reset'];
                sleep($limtReset);
            }
        }
        catch (APIRequestException $e) {
            $message = json_encode([
                    'httpCode' => $e->httpCode,
                    'code'     => $e->code,
                    'message'  => $e->message,
                    'json'     => $e->json,
                    'rateLimitLimit'     => $e->rateLimitLimit,
                    'rateLimitRemaining' => $e->rateLimitRemaining,
                    'rateLimitReset'     => $e->rateLimitReset
                ]);
        }
        catch (APIConnectionException $e) {
            $message = json_encode([
                'message'           => $e->getMessage(),
                'isResponseTimeout' => $e->isResponseTimeout
            ]);
        }

        return [
            'status'  => $status,
            'data'    => $data,
            'message' => $message
        ];
    }

    /**
     * 检查发送条数
     * @param int $pushId
     * @param int $userId
     * @return array
     * @author xi
     */
    private function checkSendCount($pushId,$userId,$from)
    {
        $cacheOneCount   = 'pushcacheonecount'.$from.$pushId.$userId;
        $cacheThreeCount = 'pushcachethreecount'.$from.$userId;

        $cacheTime = strtotime('tomorrow') - time();

        if(Yii::$app->cache->exists($cacheThreeCount))
        {
            $count = Yii::$app->cache->get($cacheThreeCount);
            if( $count >= 3 ){
                return [
                    'status'  => 0,
                    'message' => '一天只能发送3条'
                ];
            }
            Yii::$app->cache->set($cacheThreeCount, $count+1,$cacheTime);
        }
        else{
            Yii::$app->cache->set($cacheThreeCount, 1,$cacheTime);
        }

        if(!Yii::$app->cache->exists($cacheOneCount)){
            Yii::$app->cache->set($cacheOneCount, 1,$cacheTime);
        }
        else {
            return [
                'status'  => 0,
                'message' => 'push id = '.$pushId . ', 已发送过了'
            ];
        }

        return [
            'status'  => 1,
            'message' => 'success'
        ];
    }

	/**
     * 日志记录
     * @param stirng $message
     * @author xi
     */
    private function logs($message) {
		$file = '/tmp/logs/'.date('Ym').'/push.log';
		if( !file_exists(dirname($file))){
			@mkdir(dirname($file),0777,true);
		}
        @file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] ", FILE_APPEND);
        @file_put_contents($file, $message . " \n", FILE_APPEND);
    }

    /**
     * 打卡推送
     * @author xi
     */
    public function actionStartPunchCardPush($page,$pageSize)
    {
        ini_set('memory_limit', '2048M');
        date_default_timezone_set("Asia/Shanghai");

        $where = [
            'is_send' => 0,
        ];
        $data = PunchCardPush::find()
                ->where($where)
                ->andWhere(['<=','push_time',date('Y-m-d H:i:s')])
                ->offset(($page-1)*$pageSize)
                ->limit($pageSize)
                ->orderBy('id asc')
                ->asArray()
                ->all();

        sleep(2);
        if($data)
        {
            foreach ($data as $val)
            {
                //ios 推送
                $iosRes = $this->sendSingle($val['user_id'], $val['push_text'], $val['object_type'], $val['object_id'],'ios');

                $attributes = [
                    'jg_ios_msg_id'      => $iosRes['data']['msg_id'],
                    'xm_id'              => 0,
                    'is_send'            => 1,
                    'update_time'        => date('Y-m-d H:i:s')
                ];
                $where = ['id' => $val['id']];
                PunchCardPush::updateAll($attributes,$where);
            }
        }
    }

    /**
     * 推送
     */
    public function actionPush()
    {
        $where = [
            'is_send'  => 0
        ];
        $count = PunchCardPush::find()
            ->where($where)
            ->andWhere(['<=','push_time',date('Y-m-d H:i:s')])
            ->count();
        if($count > 0)
        {
            $pageSize = ceil($count/5);

            $path = dirname(__FILE__);
            $cmd = "sh " . $path . "/punchcardpush.sh 5 $pageSize";
            exec($cmd);
        }
    }
}
