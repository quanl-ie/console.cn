<?php
namespace console\controllers;

use console\models\Command;
use console\models\Work;
use console\models\WorkRelTechnician;
use yii\console\Controller;
use Yii;

class CommandController extends Controller
{
	/**
     * 指挥中心初始化数据
     * crontab
     * @author liquan
     */
	public function actionInit()
	{
        Command::initData();
	}
    /**
     * 指挥中心每日修正数据
     * crontab
     * @author liquan
     */
	public function actionRepair()
    {
        Command::repair();
    }

}


