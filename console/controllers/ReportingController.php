<?php
namespace console\controllers;

use console\models\Work;
use console\models\WorkRelTechnician;
use yii\console\Controller;
use Yii;

class ReportingController extends Controller
{
	/**
     * 生成报表
     * crontab
     * @author liquan
     */
	public function  actionAutoReporting()
	{
	    try
        {
            //平台派单量
            Work::companyOrder();
            //技师接单统计
            WorkRelTechnician::technicianOrder();
            //客户派单统计
            Work::accountOrder();
        }
        catch (\Exception $e)
        {
            echo $e->getTraceAsString() ."\n";
        }
	}

}


