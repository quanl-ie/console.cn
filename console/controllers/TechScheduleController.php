<?php
namespace console\controllers;

use common\models\ScheduleTechnician;
use Yii;
use yii\console\Controller;
use console\models\HistoryPosition;
use common\helpers\QueuePush;
use common\models\Technician;
use  console\models\WorkProcess;

/*
 * @desc 技师排班日历订单更新表
 *
 *
 */
class TechScheduleController extends Controller
{
    /**
     * @dessc 已完成、所有订单同步到技师排班表 被执行的job,待指派时调用
     * @param
     * @author chengjuanjuan
     * @param
     * @return string
     */
    public function actionIncrease($tid='',$status=2,$departmentTopId='',$departmentId='') {
        self::actionData(2,$tid);
        self::actionData(4,$tid);
    }


    /**
     * @desc 工单改派or 取消,原有工单总数-1
     * @param $departmentTopId
     * @param $departmentId
     * @return string
     */
    public function actionReduce($tid='',$departmentTopId,$departmentId) {
        self::actionData(2,$tid);
        self::actionData(4,$tid);
    }



    public function ReturnCode($code,$msg){
        return json_encode(['code'=>$code,'msg'=>$msg]);
    }


    /**
     * 测试数据
     * redis,第三个参数为数组，接收的控制器当成2个变量接收，见index方法
     * @param $tid
     * @param $status
     * @param $orgType
     * @param $orgId
     * @param int $isAssign
     * @param int $lastTid
     */
    public function actionTest($tid,$status,$orgType,$orgId,$isAssign=0,$lastTid=0){
        QueuePush::sendPush('increase','tech-schedule','increase',[[4],2,13,5]);

    }


    /**author chengjuanjuan
     * @param int $status
     * 订单数据初始化
     * status=4,已完成
     *全部订单执行命令： php yii tech-schedule/data 2
     *完成订单执行命令： php yii tech-schedule/data 4
     *
     */
    public function actionData($status = 2,$technicianId = 0){
        if($technicianId){
            $techId = Technician::findAll(['audit_status'=>5,'status'=>1,'id'=>$technicianId]);
        }else{
            $techId = Technician::findAll(['audit_status'=>5,'status'=>1]);
        }
        foreach($techId as $v){
            $tid = $v['id'];
            $departmetnId =  $v['store_id'];
            $departmentTopId = $v['department_top_id'];
            $model = new ScheduleTechnician();
            if($status==2){
                $workList = WorkProcess::getTechScheduleData($tid);
            }else{
                $workList = WorkProcess::getTechScheduleData($tid,1);
            }
            if(empty($workList)){
                continue;
            }
            foreach($workList as $vv){
                $applyDate = date('Y-m-d 00:00:00',strtotime($vv['apply_date']));
                $info = ScheduleTechnician::findOne(['tech_id'=>$tid,'department_id'=>$departmetnId,'apply_date'=>$applyDate]);
                if($info){
                    if($status==4){
                        $info->finish_order = $vv['count'];
                    }else{
                        $info->all_order    = $vv['count'];
                    }
                    $res = $info->save(false);;
                }else{
                    $model = new ScheduleTechnician();
                    $model->tech_id          = $tid;
                    if($status==4){
                        $model->finish_order = $vv['count'];
                    }else{
                        $model->all_order        =  $vv['count'];;
                    }
                    //$info->department_top_id = $departmentTopId;
                    $model->department_id        = $departmetnId;
                    $model->apply_date           = $applyDate;
                    $res = $model->save(false);
                }

            }


        }
    }


}


