<?php
namespace console\controllers;

use yii\console\Controller;
use console\models\Role;

class RolesController extends Controller
{
    
    /**
     * 初始化权限
     * @author xi
     * @date 2018-5-16
     */
    public function actionInit()
    {
        echo "begin \n";
        Role::initDefault();
        echo "end \n";
    }
    
}