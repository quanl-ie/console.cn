<?php
namespace console\controllers;

use console\models\WorkOrder;
use console\models\WorkOrderDynamic;
use Yii;
use yii\console\Controller;
use console\models\Order;
use console\models\Technician;
use console\models\Message;

class WorkOrderDynamicController extends Controller
{
	
	/**
     * 动态的 content
     * crontab
     */
	public function  actionReplaceContentSj()
	{
	    $cache = "WorkOrderDynamicSj";

	    try
        {
            if(!Yii::$app->cache->exists($cache))
            {
                Yii::$app->cache->set($cache,1,60);

                echo date('Y-m-d H:i:s') . "\n";
                WorkOrderDynamic::changeDynamic();
                echo date('Y-m-d H:i:s') . "\n";

                Yii::$app->cache->delete($cache);
            }
        }
        catch (\Exception $e)
        {
            echo $e->getTraceAsString() ."\n";
            Yii::$app->cache->delete($cache);
        }
	}

}


