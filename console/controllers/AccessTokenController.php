<?php
namespace console\controllers;

use common\lib\openapi\WeChatAuthorization;
use console\models\MiniProgram;
use Yii;
use yii\console\Controller;

class AccessTokenController extends Controller
{

    /**
     * 定时刷新token
     */
	public function  actionStart()
	{
	    $where = [
	        'is_authorization' => 1,
            'status' => 1
        ];
	    $query = MiniProgram::find()->where($where)->select('id,appid,refresh_token')->asArray()->all();
	    if($query)
        {
            foreach ($query as $val)
            {
                $res = WeChatAuthorization::apiAuthorizerToken($val['appid'],$val['refresh_token']);
                if(isset($res['authorizer_access_token']))
                {
                    $attr = [
                        'access_token'  => $res['authorizer_access_token'],
                        'expires_in'    => $res['expires_in'] + time(),
                        'refresh_token' => $res['authorizer_refresh_token']
                    ];
                    MiniProgram::updateAll($attr,['id'=>$val['id']]);
                }
            }
        }

        echo date('Y-m-d H:i:s')."\n";
	}
}


