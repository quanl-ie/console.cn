<?php
namespace console\controllers;

use console\models\TechnicianCostRule;
use console\models\WorkCostDivide;
use Yii;
use yii\console\Controller;
use console\models\Order;

class WorkCostDivideController extends Controller
{
	
	/**
     * 工单结算脚本
     * crontab
     * @author xi
     */
	public function  actionStart()
	{
	    $cache = "WorkCostDivideController";

	    try
        {
            if(1 || !Yii::$app->cache->exists($cache))
            {
                Yii::$app->cache->set($cache,1,3600*5);

                echo date('Y-m-d H:i:s') . "\n";


                $res = WorkCostDivide::add();
                var_dump($res);


                echo date('Y-m-d H:i:s') . "\n";

                Yii::$app->cache->delete($cache);
            }
        }
        catch (\Exception $e)
        {
            echo $e->getTraceAsString() ."\n";
            Yii::$app->cache->delete($cache);
        }
	}
}


