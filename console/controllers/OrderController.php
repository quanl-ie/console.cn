<?php
namespace console\controllers;

use console\models\WorkOrder;
use Yii;
use yii\console\Controller;
use console\models\Order;
use console\models\Technician;
use console\models\Message;

class OrderController extends Controller
{
	
	/**
     * 订单超时自动取消
     * crontab
     * @author xi
     */
	public function  actionAutoCancelOrder()
	{
	    $cache = "ordercancel";
	    try
        {
            if(!Yii::$app->cache->exists($cache))
            {
                  Yii::$app->cache->set($cache,1,3600*5);
                  //指派超时
                  WorkOrder::assignTimeout(0);
                  //上门超时
                  WorkOrder::serviceTimeout();
                  //30分提醒上门服务 发消息
                  WorkOrder::serviceTip(0);
                  Yii::$app->cache->delete($cache);
                  echo "执行完毕。 ".date('Y-m-d H:i:s')."\r\n";
                //echo date('Y-m-d H:i:s') . "\n";
                //立即上门时间 15 分钟
                //WorkOrder::cancelOrder(0,900);
                //预约上门  30 分钟
                //WorkOrder::cancelOrder(0,1800);

            }
        }
        catch (\Exception $e)
        {
            echo $e->getTraceAsString() ."\n";
            Yii::$app->cache->delete($cache);

        }

	}

}


