<?php
namespace console\controllers;

use console\models\Calc;
use console\models\WorkOrder;
use Yii;
use yii\console\Controller;

class CalcController extends Controller
{
	
	/**
     * 订单超时自动取消
     * crontab
     * @author xi
     */
	public function  actionAutoCalcOrder()
	{
	    echo "begin  ";

	    Calc::syncData();
	    echo "end  ";
	}


}


