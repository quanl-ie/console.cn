<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use console\models\HistoryPosition;

class PositionController extends Controller
{
	/**
     * 位置信息出队入库
     */
    public function actionPositionPop() {
        while (true) {
            $redis = Yii::$app->redis;     
            $str = $redis->blpop('position',0);
            HistoryPosition::savePosition($str);
        }
    }
    
}


