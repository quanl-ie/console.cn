<?php
namespace console\controllers;

use common\lib\openapi\MiniProgramCode;
use console\models\MiniProgram;
use Yii;
use yii\console\Controller;

class XcxController extends Controller
{

    /**
     * 查询审核状态
     */
	public function  actionAudit()
	{
        $where = 'auditid >0 and audit_status = 4';
        $query = MiniProgram::find()->where($where)->select('id,access_token,auditid')->asArray()->all();
        if($query)
        {
            foreach ($query as $val)
            {
                $res = MiniProgramCode::getAuditStatus($val['access_token'],$val['auditid']);
                if( isset($res['errcode']) && $res['errcode'] == 0)
                {
                    $weixinAuditStatus = 0;
                    $weixinAuditReason = '';
                    $auditStatus = 0;

                    //审核通过
                    if($res['status'] == 0){
                        $weixinAuditStatus = 2;
                        $res1 = MiniProgramCode::release($val['access_token']);
                        if($res1['errcode'] == 0){
                            $auditStatus = 5;
                        }
                    }
                    if($res['status'] == 1){
                        $weixinAuditStatus = 3;
                        $weixinAuditReason = $res['reason'];
                    }
                    if($res['status'] == 2){
                        $weixinAuditStatus = 1;
                    }

                    $attr = [
                        'weixin_audit_status'=>$weixinAuditStatus,
                        'weixin_audit_reason'=>$weixinAuditReason
                    ];
                    if($auditStatus >0){
                        $attr['audit_status'] = 5;
                    }
                    MiniProgram::updateAll($attr,['id'=>$val['id']]);
                }
            }
        }

        echo date('Y-m-d H:i:s') . "\n";

	}
}


