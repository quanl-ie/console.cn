<?php
namespace console\models;

use Yii;

class ScheduleTechnicianQueue extends BaseModel
{

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'schedule_technician_queue';
    }

    /**
     * 获取队列数据
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function getAllData($limit = 1000)
    {
        $query = self::find()
            ->where(['status'=>1])
            ->asArray()
            ->all();

        if($query){
            return $query;
        }
        return [];
    }

}