<?php
namespace console\models;

use Yii;

class WorkCost extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost';
    }

    /**
     * 查出工单的收费项目
     * @param $workNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCost($workNo,$costId)
    {
        $query = self::find()
            ->where(['work_no'=>$workNo,'status'=>1,'cost_id'=>$costId])
            ->asArray()
            ->all();
        if($query)
        {
            return $query;
        }
        return [];
    }
}