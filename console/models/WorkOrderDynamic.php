<?php
namespace console\models;

use common\models\Account;
use common\models\Common;
use common\models\GlobalSocialRole;
use common\models\Order;
use console\controllers\Department;
use console\controllers\GlobalAccount;
use Yii;

class WorkOrderDynamic extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_dynamic';
    }

    /**
     * 获取动态
     * @param $type
     * @param $orderNo
     */
    public static function changeDynamic()
    {
        $where = ['status' => 2];
        $data = self::find()
            ->where($where)
            ->limit(1000)
            ->orderBy(['create_time' => SORT_DESC])
            ->asArray()
            ->all();
        if($data)
        {
            foreach ($data as $val)
            {
                $content = self::getContent($val['type_status'],$val['order_no'],$val['params'],$val['work_no']);
                
                $res = ['content'=>$content,'status'=>1,'update_time'=>time()];
                $params = json_decode($val['params'],true);
                
                if(isset($params) && !empty($params['accountId']))
                {
                    $info = common::getDirectCompanyName($params['accountId']);
                    
                    $res['account_name'] = Common::getUserName($params['accountId']);
                    $res['direct_company_name'] = !empty($info['name'])?$info['name']:'';
                    $res['direct_company_id'] = !empty($info['id'])?$info['id']:'';
                }
   
                if($content){
                    self::updateAll($res,['id'=>$val['id']]);
                }
            }
        }
    }

    /**
     * 商家动态描述
     * @param $typeStatus
     * @param $orderNo
     * @param $workNo
     * @param $params
     * @return string
     */
    private static function getContent($typeStatus,$orderNo,$params,$workNo='')
    {
        $result = '';
        $orderData = Order::findOneByOrderNo($orderNo);

        if($orderData)
        {
            $params = json_decode($params,true);
            
            switch ($typeStatus)
            {
                case 1:
                    $className = Common::getClassNameBySaleId($orderData['sale_order_id']);
                    $workName  = Common::getWorktypeName($orderData['work_type']);
                    $result    = "【%s】创建了【".$className."】【".$workName."】订单";
                    break;
                case 2:
                    $className = Common::getClassNameBySaleId($orderData['sale_order_id']);
                    $workName  = Common::getWorktypeName($orderData['work_type']);
                    $storeName = Common::getDepartmentName($params['fwsId']);
                    $result    = "【%s】创建了【".$className."】【".$workName."】订单，并指派给【".$storeName."】";
                    break;
                case 3:
                case 4:
                    $storeName   = Common::getDepartmentName($params['jgId']);
                    $result = "【%s】将订单指派给【".$storeName."】";
                    break;
                case 5:
                    $technicianRes = Common::getTechniciansInfo($params['jsId']);
                    if($technicianRes) {
                        $technicianArr = array_column($technicianRes, 'name');
                    }
                    $leaderStr = '';
                    if(!empty($params['jsLeader'])){
                        $jsLeader = Common::getTechnicianName($params['jsLeader']);
                        $leaderStr = "，负责人为【".$jsLeader."】";
                    }

                    $result = "【%s】将订单指派给【".implode(',',$technicianArr)."】".$leaderStr;
                    break;
                case 6:
                    $zpJs = Common::getTechnicianName($params['zhipaiJs']);
                    $technicianRes = Common::getTechniciansInfo($params['jsId']);
                    if($technicianRes) {
                        $technicianArr = array_column($technicianRes, 'name');
                    }

                    $leaderStr = '';
                    if(!empty($params['jsLeader'])){
                        $jsLeader = Common::getTechnicianName($params['jsLeader']);
                        $leaderStr = "，负责人为【".$jsLeader."】";
                    }

                    $result = "【".$zpJs."】将订单指派给【".implode(',',$technicianArr)."】".$leaderStr;
                    break;
                case 7:
                    //$reason = $params['reason']; //改派时候，没有改派原因
                    $storeName   = Common::getDepartmentName($params['fwsId']);
                    $result = "【%s】将订单改派给【".$storeName."】";
                    break;
                case 8:
                    $reason = isset($params['reason'])?$params['reason']:'';
                    $technicianName = Common::getTechnicianName($params['jsId']);
                    
                    if (isset($params['reason'])) {
                        $result = "【%s】因【".$params['reason']."】将订单改派给【".$technicianName."】";
                    }
                    
                    if (isset($params['zhipaiJs'])) {
                        $zhipaiJsName = Common::getTechnicianName($params['zhipaiJs']);
                        $result = "【".$zhipaiJsName."】将订单改派给【".$technicianName."】";
                    }
                    break;
                case 9:
                    $reason = $params['reason'];
                    $result = "【%s】因【".$reason."】取消了订单";
                    break;
                case 10:
                    $reason = $params['reason'];
                    $storeName = Common::getDepartmentName($params['fwsId']);
                    $result = "【%s】因【".$reason."】将订单驳回给【".$storeName."】";
                    break;
                case 11:
                    $stringTime = self::getFormatTime($params['planTimeType'],$params['planTime']);
                    $result = "【%s】修改服务时间为".$stringTime;
                    break;
                case 12:
                    if(isset($params['accountId'])){
                        $result = "【%s】已开始服务";
                    }
                    else if(isset($params['jsId'])){
                        $technicianName = Common::getTechnicianName($params['jsId']);
                        $result = "技师【".$technicianName."】已开始服务";
                    }

                    break;
                case 13:
                    $technicianName = Common::getTechnicianName($params['jsId']);
                    $result = "技师【".$technicianName."】已开始服务";
                    break;
                case 14:
                    if(isset($params['accountId'])){
                        $result = "【%s】已开始服务";
                    }
                    else if(isset($params['jsId'])){
                        $technicianName = Common::getTechnicianName($params['jsId']);
                        $result = "技师【".$technicianName."】已完成服务";
                    }
                    break;
                case 15:
                    $technicianName = Common::getTechnicianName($params['jsId']);
                    $result = "技师【".$technicianName."】已完成服务";
                    break;
                case 16:
                    $stringTime = self::getFormatTime($params['planTimeType'],$params['planTime']);
                    $workStage = Common::getServiceFlow($params['workStage']);
                    $result    = "【%s】因".$params['reason']."预约了下次服务，服务内容为【".$workStage."】，服务时间为".$stringTime;
                    break;
                case 17:
                    $stringTime = self::getFormatTime($params['planTimeType'],$params['planTime']);
                    $workStage = Common::getServiceFlow($params['workStage']);
                        
                    if ($params['jsId'] == 0) {
                        $technicianName = '';
                    }else{
                        $technicianName = Common::getTechnicianName($params['jsId']);
                    }
                    
                    if ($technicianName == '') {
                        $str = '';
                    }else{
                        $str = "技师【".$technicianName."】";
                    }
                    
                    if ($params['planTime'] == 0) {
                        $result = "因".$params['reason']."预约了下次服务，服务内容为【".$workStage."】，服务时间暂不指定";
                    }else{
                        $result = "因".$params['reason']."预约了下次服务，服务内容为【".$workStage."】，服务时间为".$stringTime;
                    }
                    
                    break;
                case 18:
                    $result = "【%s】已验收通过";
                    break;
                case 19:
                    $result = "【%s】已验收通过，订单已完成";
                    break;
                case 20:
                    $result = "【%s】验收未通过，等待服务机构重新处理订单";
                    break;
                case 21:
                    $workName  = Common::getWorktypeName($orderData['work_type']);
                    $accountArr = self::getAccountNameAndMobile( $orderData['create_user_id']);
                    $result = '【'.$accountArr['nickname'].'】的【'.$accountArr['mobile'].'】创建了【'.$workName.'】订单';
                    break;
                case 22:
                    $reason = $params['reason'];
                    $accountId = $params['accountId'];
                    $accountArr = self::getAccountNameAndMobile( $accountId );
                    $result = "【".$accountArr['nickname']."】因【".$reason."】取消了订单";
                    break;

                case 23:
                    $departmentId = $params['department_id'];
                    $userId       = $params['user_id'];

                    $departmentName = Common::getDepartmentName($departmentId);
                    $userName = Common::getUserName($userId);

                    $result = "【%s】已将订单审核通过";
                    break;

                case 24:
                    $departmentId = $params['department_id'];
                    $userId       = $params['user_id'];
                    $reason       = $params['reason'];

                    $departmentName = Common::getDepartmentName($departmentId);
                    $userName = Common::getUserName($userId);

                    $result = "【%s】因".$reason."将订单驳回";
                    break;
                default:
                    break;
            }
        }

        return $result;
    }

    /**
     * 查出服务时间
     * @param integer $plantime
     * @param integer $plantimetype
     * @return mixed|string
     */
    public static function getFormatTime($plantimetype,$plantime,$format='Y-m-d')
    {
        if(empty($plantimetype) || empty($plantime))
            return '';
        $data = [
            1 => '全天',
            2 => '上午',
            3 => '下午',
            4 => '晚上',
            5 => '具体时间'
        ];

        if($plantimetype == 5){
            return date("Y-m-d H:i",$plantime);
        }
        else {
            return date($format,$plantime) . ' '. (isset($data[$plantimetype])?$data[$plantimetype]:'');
        }
    }

    /**
     * 获取账户信息
     * @param $departmentId
     * @param $accountId
     * @return array
     */
    private static function getAccountNameAndMobile($accountId)
    {
        $result = [
            'mobile'   => '',
            'nickname' => ''
        ];

        $accountArr = Account::findOneByAttributes(['id' => $accountId]);
        if($accountArr)
        {
            $result = [
                'mobile'   => $accountArr['mobile'],
                'nickname' => $accountArr['account_name']
            ];
        }

        return $result;
    }
}