<?php
namespace console\models;
use Codeception\Module\Db;
use console\models\Work;
use console\models\WorkOrderTechnician;

use Yii;

class WorkProcess extends BaseModel
{
    public static function getDb ()
    {
        return Yii::$app->order_db;
    }

    public static function tableName ()
    {
        return 'work_process';
    }


    public static function getTechScheduleData ($techId=70,$isFinish = 0)
    {
        $db = new self();
        if($isFinish){
            //$where = ' and type=6 ';//已收款完成
            $where = ' and status = 4';
        }else{
            $where = ' ';
        }

/*   $sql = 'SELECT
    technician_id as tech_id,FROM_UNIXTIME(`create_time`) as apply_date,count(`id`) as count  FROM   '.Work::tableName().'  as a 
   WHERE 
EXISTS
    (select b.create_time from    '.self::tableName().'  as b where a.work_no =b.work_no '.$where.'  order by id desc limit 1 )

 AND (`technician_id`='.$techId.')
  AND (`cancel_status`=1) 
GROUP BY substr(FROM_UNIXTIME(`create_time`),1,10);';*/
        $sql = 'SELECT 
    b.technician_id as tech_id,FROM_UNIXTIME(`process_time`) as apply_date,count(a.`id`) as count  FROM   '.Work::tableName().'  as a 
    join ' .WorkOrderTechnician::tableName(). ' as b  on a.work_no=b.work_no
   WHERE 
 (b.technician_id='.$techId.') 
 '.$where.'
  AND (`cancel_status`=1)   and is_self <> 2 
  AND (b.type=1)
GROUP BY substr(FROM_UNIXTIME(`process_time`),1,10)';
        $db = new self();
        $result = Yii::$app->order_db->createCommand($sql)->queryAll();
        return $result;
    }

    /**
     * 获取工单状态时间
     * @param $workNo
     * @param $type
     * @return int|mixed
     */
    public static function getProcessTimeByType($workNo,$type)
    {
        $where = [
            'work_no' => $workNo,
            'type'    => $type
        ];
        $query = self::find()
            ->where($where)
            ->select("create_time")
            ->asArray()
            ->one();
        if($query)
        {
            return $query['create_time'];
        }
        return 0;
    }
}



