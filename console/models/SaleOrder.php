<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/6/25
 * Time: 16:17
 */
namespace console\models;
use Yii;

class SaleOrder extends BaseModel
{
      public static function getDb()
      {
            return Yii::$app->db;
      }
      public static function tableName()
      {
            return 'sale_order_view';
      }
      //查询类目，服务类型
      public static function getProdInfo($sale_order_id){
            $where = [
                  "id" =>$sale_order_id
            ];
            $query = self::find()
                  ->where($where)
                  ->select('type_name,class_name,prod_name')
                  ->asArray()
                  ->one();
            return $query;
      }
}