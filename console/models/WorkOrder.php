<?php
namespace console\models;

use common\helpers\Helper;
use common\lib\Push;
use common\models\WorkOrderDetail;
use Yii;

class WorkOrder extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }
    
    /**
     * 支付类型：1：支付宝，2：微信，3：线下
     * @param unknown $index
     */
    public static function getPayType($index)
    {
        $data = [
            1 => '支付宝',
            2 => '微信',
            3 => '线下'
        ];
        return isset($data[$index])?$data[$index]:'';
    }
    
    /**
     * 订单状态信息 (1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消)
     * @param int $index
     * @return string
     * @author xi
     * @date 2017-12-19
     */
    public static function getStatus($index=false)
    {
        $data = [
            1 => '待接单',
            2 => '待指派',
            3 => '待服务',
            4 => '服务中',
            5 => '已完成',
            6 => '已取消'
        ];
        if($index!=false){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }
    
    /**
     * 订单取消类型(1商家取消 2服务商取消 3门店取消 4 用户取消 5 优服务平台取消 6 无人接单系统取消7服务商拒接)
     * @param int $index
     * @return string
     * @author xi
     * @date 2017-12-19
     */
    public static function getCancelType($index)
    {
        $data = [
            1 => '商家取消',
            2 => '服务商取消',
            3 => '门店取消',
            4 => '用户取消',
            5 => '平台取消',
            6 => '系统自动取消',
            7 => '服务商拒接'
        ];
        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 自动取消订单超时XX分钟的
     * @param $maxId
     * @author  xi
     * @date 2017-12-22
     */
    public static function cancelOrder($maxId,$second = 900)
    {
        $where = 'status = 1 and cancel_status!=6 and is_receive = 1 and id > '.$maxId." and  (".time()." - create_time) > $second";
        $query = self::find()
            ->where($where)
            ->select('id,order_no')
            ->limit(1000)
            ->orderBy('id asc')
            ->asArray()
            ->all();
        if($query)
        {
            foreach ($query as $val)
            {
                $attributes = [
                    'update_time'   => time(),
                    'cancel_status' => 6
                ];
                self::updateAll($attributes,['id'=>$val['id']]);
            }

            $tempMaxId = end($query)['id'];
            echo $tempMaxId . "\n";
            self::cancelOrder($tempMaxId,$second);
        }
    }

    /**
     * 指派超时更改状态
     * @param $maxId
     * @author  xi   modify liquan
     * @date 2017-12-22
     */
    public static function assignTimeout($maxId)
    {
          $where = 'a.status = 1 and a.id > '.$maxId." and d.push_status=1 and a.plan_time -".time()." <= 1800";
          $query = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrderDetail::tableName().'` as c'], 'a.order_no = c.order_no')
                ->leftJoin(['`'.WorkOrderTechnician::tableName().'` as d'], 'a.work_no = d.work_no')
                ->where($where)
                ->groupBy('a.work_no')
                ->select('a.id,a.order_no,a.work_no,a.plan_time,a.plan_time_type,c.sale_order_id,count(*) as count')
                ->asArray()->all();
          // modify liquan
          if ($query) {
                foreach ($query as $val) {
                      //查询技师
                      $tech_ids = array();
                      $tech_info = WorkOrderTechnician::getTechnicianByWorkId($val['work_no']);
                      if (!empty($tech_info)) {
                            foreach ($tech_info as $v) {
                                  if($v['is_self'] == 2){
                                        $tech_ids[] = $v['technician_id'];
                                  }
                            }
                      } else { //如果没有技师 直接查询下一个工单
                            continue;
                      }
                      //（ 去重）如果是同一个人 只发一遍
                      $tech_ids = array_unique($tech_ids);
                      $title = "服务即将开始！请尽快指派上门技师！";
                      $type = 1;
                      $type_content['order_no'] = $val['order_no'];
                      $type_content['work_no'] = $val['id'];

                    //查询产品信息
                    $prod = SaleOrder::getProdInfo($val['sale_order_id']);
                    //拼接内容
                    $content = '';
                    if($val['plan_time'] != null){
                        if($val['plan_time_type'] == 1){
                            $content .= date("Y-m-d", $val['plan_time'])."  全天";
                        }else if($val['plan_time_type'] == 2){
                            $content .= date("Y-m-d", $val['plan_time'])."  上午";
                        }else if($val['plan_time_type'] == 3){
                            $content .= date("Y-m-d", $val['plan_time'])."  下午";
                        }else if($val['plan_time_type'] == 4){
                            $content .= date("Y-m-d", $val['plan_time'])."  晚上";
                        }else{
                            $content .= date("Y-m-d H:i", $val['plan_time']);
                        }
                    }
                    //服务类型
                    if($prod['type_name']){
                        $content .= " | " . $prod['type_name'];
                    }
                    //产品名称
                    if($prod['prod_name']){
                        if($val['count']>1){
                            $content .= " | " . $prod['prod_name']."等".$val['count']."个产品";
                        }else{
                            $content .= " | " . $prod['prod_name'];
                        }
                    }
                      foreach ($tech_ids as $m) {
                            $result = Push::pushMsg($title, $content, $m, $type, (object)$type_content,1);
                            if (json_decode($result)->success) {
                                  //更改状态 已推送
                                  WorkOrderTechnician::changePushStatus($m,$val['work_no']);
                            }
                      }
                }
          }
        /*$where = 'status = 2 and cancel_status<>8 and id > '.$maxId." and  ((service_time_type = 1 and update_time - ".time()." <= 600 ) or service_time_type = 2 and update_time - ".time()." <= 1800 )";
        $query = self::find()
            ->where($where)
            ->select('id,order_no')
            ->limit(1000)
            ->orderBy('id asc')
            ->asArray()
            ->all();
        if($query)
        {
            foreach ($query as $val)
            {
                $attributes = [
                    'update_time'   => time(),
                    'cancel_status' => 8
                ];
                if(self::updateAll($attributes,['id'=>$val['id']])){
                    //过程表加上处理过程
                    $url = Yii::$app->params['order.uservices.cn']."/v1/order/process";
                    $postData = [
                        'order_no' => $val['order_no'],
                        'src_type' => 99,
                        'src_id'   => -1,
                        'type'     => 26,
                        'process_result'  => -1,
                        'problem_reason'  => '指派超时',
                        'process_user_id' => -1,
                    ];
                    $jsonStr = Helper::curlPostJson($url,$postData);
                    $result = json_decode($jsonStr,true);
                    if(isset($result['success']) && $result['success'] == 1)
                    {
                        self::logs("订单号：".$val['order_no'].";指派超时状态更新成功",'order.log');
                    }
                    else {
                        self::logs("订单号：".$val['order_no'].";指派超时状态更新失败;返回结果:".$jsonStr,'order.log');
                    }
                }
            }

            $tempMaxId = end($query)['id'];
            echo $tempMaxId . "\n";
            self::assignTimeout($tempMaxId);*/

    }

    /**
     * 上门超时更改状态
     * @param $maxId
     * @author  xi   modify liquan
     * @date 2017-12-22
     */
    public static function serviceTimeout()
    {

        $where = "a.is_assign_technician = 1 and a.plan_time <= ".time()." and b.status = 2 and d.status = 2 and t.push_status = 1";
        $query = self::find()
            ->from(self::tableName() . ' as a ')
            ->innerJoin(['`'.WorkOrderStatus::tableName().'` as b'], 'a.order_no = b.order_no')
            ->leftJoin(['`'.Work::tableName().'` as d'], 'a.order_no = d.order_no')
            ->leftJoin(['`'.WorkOrderTechnician::tableName().'` as t'], 'd.work_no = t.work_no')
            ->leftJoin(['`'.WorkOrderDetail::tableName().'` as c'], 'a.order_no = c.order_no')
            ->where($where)
            ->groupBy('d.work_no')
            ->select('b.id,a.order_no,a.plan_time,a.plan_time_type,c.sale_order_id,d.work_no,count(*) as count')
            ->asArray()->all();
        if($query)
        {
               $ids = array_column($query,'id');
               WorkOrderStatus::updateAll(['status'=>9,'update_time'=>time(),'remarks'=>'上门超时'] , ['id'=>$ids]);
            foreach ($query as $val)
            {
                  WorkOrderProcess::add($val['order_no'],9,'上门超时');
                  //查询技师
                  $tech_ids = array();
                  $tech_info = WorkOrderTechnician::getTechnicianByWorkId($val['work_no']);
                  if(!empty($tech_info))
                  {
                        foreach($tech_info as $v)
                        {
                              //有组长的情况
                              if($v['is_self'] == 2)
                              {
                                    //判断有没有指派
                                    $resa =  WorkOrderTechnician::getTechnicianByWorkId($v['work_no']);
                                    $zp_arr_ids = array_column($resa,'is_self');
                                    // 有技师
                                    if(in_array(3,$zp_arr_ids))
                                    {
                                          foreach($resa as $vak)
                                          {
                                                // 无论有没有组长 都发送给负责人和技师
                                                if($vak['is_self'] == 1 || $vak['is_self'] == 3){
                                                      $tech_ids[] = $vak['technician_id'];
                                                }
                                          }
                                    }else  //无技师 发给组长
                                    {
                                          foreach($resa as $vak)
                                          {
                                                if($vak['is_self'] == 2){
                                                      $tech_ids[] = $vak['technician_id'];
                                                }
                                          }
                                    }
                              }else{
                                    // 无组长的情况 肯定有负责人或者技师  直接遍历
                                    //（$tech_info 已经包含了 所有type类型 1,2,3，排除 2 组长 剩下的就是 1,3 直接遍历）
                                    $tech_ids[] = $v['technician_id'];
                              }
                        }
                  }else{  //如果没有技师 直接查询下一个工单
                        continue;
                  }
                $title        = "【客户催单】服务时间已到，请尽快上门！";
                $type         = 1;
                $type_content['order_no'] = $val['order_no'];
                $type_content['work_no'] = $val['id'];
                //查询产品信息
                $prod = SaleOrder::getProdInfo($val['sale_order_id']);
                //拼接内容
                $content = '';
                if($val['plan_time'] != null){
                    if($val['plan_time_type'] == 1){
                        $content .= date("Y-m-d", $val['plan_time'])."  全天";
                    }else if($val['plan_time_type'] == 2){
                        $content .= date("Y-m-d", $val['plan_time'])."  上午";
                    }else if($val['plan_time_type'] == 3){
                        $content .= date("Y-m-d", $val['plan_time'])."  下午";
                    }else if($val['plan_time_type'] == 4){
                        $content .= date("Y-m-d", $val['plan_time'])."  晚上";
                    }else{
                        $content .= date("Y-m-d H:i", $val['plan_time']);
                    }
                }
                //服务类型
                if($prod['type_name']){
                    $content .= " | " . $prod['type_name'];
                }
                //产品名称
                if($prod['prod_name']){
                    if($val['count']>1){
                        $content .= " | " . $prod['prod_name']."等".$val['count']."个产品";
                    }else{
                        $content .= " | " . $prod['prod_name'];
                    }
                }
                //（ 去重）如果是同一个人 只发一遍
                $tech_ids = array_unique($tech_ids);

                foreach($tech_ids as $m){
                      $result = Push::pushMsg($title,$content,$m,$type,(object)$type_content,2);
                      if(json_decode($result)->success){
                         //更改状态 已推送
                          WorkOrderTechnician::changePushStatus($m,$val['work_no']);
                          file_put_contents("/tmp/order_time_out.log","时间:".date("Y-m-d H:i:s",time())."推送消息成功 工单号：".$val['work_no']."技师id:".$m.PHP_EOL, FILE_APPEND);
                      }else{
                          file_put_contents("/tmp/order_time_out.log","时间:".date("Y-m-d H:i:s",time())."推送消息失败 工单号：".$val['work_no']."技师id:".$m.PHP_EOL, FILE_APPEND);
                      }
                }
            }

        }
    }

    /**
     * 服务提醒推送
     * @param $maxId
     * @author  xi  modify liquan
     * @date 2017-12-22
     */
    public static function serviceTip($maxId)
    {
          $where = 'a.status = 2 and a.id > '.$maxId." and  a.plan_time -".time()." <= 1800 and b.push_status = 1";
          $query = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrderDetail::tableName().'` as c'], 'a.order_no = c.order_no')
                ->leftJoin(['`'.WorkOrderTechnician::tableName().'` as b'], 'a.work_no = b.work_no')
                ->where($where)
                ->groupBy('a.work_no')
                ->select('a.id,a.order_no,a.work_no,a.plan_time,a.plan_time_type,c.sale_order_id,count(*) as count')
                ->asArray()->all();
          // modify liquan
          if ($query) {
                foreach ($query as $val) {
                      //查询技师
                      $tech_ids = array();
                      $tech_info = WorkOrderTechnician::getTechnicianByWorkId($val['work_no']);
                      if (!empty($tech_info)) {
                            foreach ($tech_info as $v) {
                                  if ($v['is_self'] == 1 || $v['is_self'] == 3) {
                                        $tech_ids[] = $v['technician_id'];
                                  }
                            }
                      } else { //如果没有技师 直接查询下一个工单
                            continue;
                      }
                      //（ 去重）如果是同一个人 只发一遍
                      $tech_ids = array_unique($tech_ids);
                      $title = "服务即将开始！您准备好了吗？";
                      $type = 1;
                      $type_content['order_no'] = $val['order_no'];
                      $type_content['work_no'] = $val['work_no'];
                    //查询产品信息
                    $prod = SaleOrder::getProdInfo($val['sale_order_id']);
                    //拼接内容
                    $content = '';
                    if($val['plan_time'] != null && $val['plan_time'] != 0){
                        if($val['plan_time_type'] == 1){
                            $content .= date("Y-m-d", $val['plan_time'])."  全天";
                        }else if($val['plan_time_type'] == 2){
                            $content .= date("Y-m-d", $val['plan_time'])."  上午";
                        }else if($val['plan_time_type'] == 3){
                            $content .= date("Y-m-d", $val['plan_time'])."  下午";
                        }else if($val['plan_time_type'] == 4){
                            $content .= date("Y-m-d", $val['plan_time'])."  晚上";
                        }else{
                            $content .= date("Y-m-d H:i", $val['plan_time']);
                        }
                    }
                    //服务类型
                    if($prod['type_name']){
                        $content .= " | " . $prod['type_name'];
                    }

                    //产品名称
                    if($prod['prod_name']){
                        if($val['count']>1){
                            $content .= " | " . $prod['prod_name']."等".$val['count']."个产品";
                        }else{
                            $content .= " | " . $prod['prod_name'];
                        }
                    }
                      foreach ($tech_ids as $m) {

                            $result = Push::pushMsg($title, $content, $m, $type, (object)$type_content,2);
                            if (json_decode($result)->success) {
                                  //更改状态 已推送
                                  WorkOrderTechnician::changePushStatus($m,$val['work_no']);
                            }
                      }
                }
                // xi
                /*if($query)
                {
                    foreach ($query as $val)
                    {
                        $cacheKey = "tip".$val['work_no'];
                        if(!Yii::$app->cache->exists($cacheKey))
                        {
                            $url = Yii::$app->params['order.uservices.cn'] . "/v1/order/send-message";
                            $postData = [
                                'order_no' => $val['order_no'],
                                'work_no'  => $val['work_no']
                            ];
                            $jsonStr = Helper::curlPostJson($url, $postData);
                            $result = json_decode($jsonStr, true);
                            if (isset($result['success']) && $result['success'] == 1) {
                                self::logs("工单号：" . $val['work_no'] . ";上门提醒消息发送成功", 'order.log');
                            } else {
                                self::logs("工单号：" . $val['work_no'] . ";上门提醒消息发送成功失败;返回结果:" . $jsonStr, 'order.log');
                            }

                            Yii::$app->cache->set($cacheKey,1,86400);
                        }
                    }

                    $tempMaxId = end($query)['id'];
                    echo $tempMaxId . "\n";
                    self::serviceTip($tempMaxId);
                }*/
          }
    }
}