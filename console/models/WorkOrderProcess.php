<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class WorkOrderProcess extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_process';
    }

    /**
     * 订单号
     * @param $orderNo
     * @param $type
     * @return bool
     * @author xi
     * @date 2018-6-15
     */
    public static function add($orderNo,$status,$remark)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            self::updateAll(['current'=>0],['order_no'=>$orderNo]);

            $model = new self();
            $model->order_no    = $orderNo;
            $model->status      = $status;
            $model->current     = 1;
            $model->remark      = $remark;
            $model->create_time = time();
            if($model->save())
            {
                $transaction->commit();
                return true;
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            throw new $e;
        }

        $transaction->rollBack();
        throw new \Exception(self::className() ."保存失败");
    }

}