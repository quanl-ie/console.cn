<?php
namespace console\models;
use Codeception\Module\Db;
use console\models\Work;
use console\models\WorkOrderTechnician;

use Yii;

class ReportingTechnicianOrder extends BaseModel
{
    public static function getDb ()
    {
        return Yii::$app->db;
    }

    public static function tableName ()
    {
        return 'reporting_technician_order';
    }
    /**
     * 获取单条信息
     */
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }
    /**
     * 批量添加
     */
    public static function addBatch($key,$value)
    {
        return Yii::$app->db->createCommand()->batchInsert(self::tableName(),$key,$value)->execute();

    }
    /**
     * 单条添加
     */
    public static function add($data)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (isset($data['id'])) {
                $model = self::findOne(['id' => $data['id']]);
            } else {
                $model = new self();
            }

            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            if ($model->save()) {
                $transaction->commit();
                return true;
            }
        } catch (\Exception $e)
        {
            $transaction->rollBack();
            throw new $e;
        }
        $transaction->rollBack();
        throw new \Exception(self::className() ."保存失败");
    }
}



