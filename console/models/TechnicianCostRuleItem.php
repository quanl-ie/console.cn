<?php
namespace console\models;

use Yii;

class TechnicianCostRuleItem extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_cost_rule_item';
    }
}