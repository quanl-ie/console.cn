<?php
namespace console\models;

use Yii;

class MiniProgram extends BaseModel
{

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'mini_program';
    }


}