<?php
namespace console\models;

use Yii;

class TechnicianCostRule extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_cost_rule';
    }

    /**
     * 查出服务商与技师合作规则
     * @param $fwsId
     * @param $jsId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCostRule($fwsId,$jsId)
    {
        $where = [
            'b.`status`'      => 1,
            'a.src_type'      => \common\models\BaseModel::SRC_FWS,
            'b.src_id'          => $fwsId,
            'a.technician_id' => $jsId
        ];

        $query = self::find()
            ->from(self::tableName() . ' as b')
            ->innerJoin(['`'. TechnicianCostRuleRelation::tableName() . '` as a' ], ' a.tech_cost_rule_id = b.id ')
            ->innerJoin(['`'. TechnicianCostRuleItem::tableName() .'` as c'],' b.id = c.tech_cost_rule_id ')
            ->where($where)
            ->select(' a.technician_id,c.cost_item_id,c.cost_type,c.fixed_amount,c.pecent')
            ->asArray()
            ->all();
        if($query)
        {
            return $query;
        }
        return [];
    }
}