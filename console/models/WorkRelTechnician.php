<?php
namespace console\models;

use common\models\Department;
use Yii;

class WorkRelTechnician extends BaseModel
{
    public static function getDb ()
    {
        return Yii::$app->order_db;
    }

    public static function tableName ()
    {
        return 'work_rel_technician';
    }

    public static function getAllDataByWorkNos($workNos)
    {
        $where = [
            'work_no' => $workNos,
            'type'    => 1,
            'is_self' => [1,3]
        ];
        $query = self::find()
            ->where($where)
            ->select('work_no,technician_id,is_self')
            ->asArray()
            ->all();
        if($query)
        {
            $result = [];
            foreach ($query as $val)
            {
                $result[$val['work_no']][] = $val;
            }
            return $result;
        }
        return [];
    }

    /**
     * 获取技师id
     * @param $workNo
     * @param $type
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function getTechnicianIds($workNo,$type)
    {
        $where = [
            'is_self' => [1,3],
            'type'    => $type,
            'work_no' => $workNo
        ];
        $query = self::find()
            ->where($where)
            ->select("technician_id,type")
            ->asArray()
            ->all();

        if($query)
        {
            return $query;
        }
        return [];
    }
    public static function getName($query){
        $tec_ids = array_unique(array_column($query,'technician_id'));
        $res = Technician::getTechnicianInfo($tec_ids);
        foreach ($res as $v){
            $result[$v['id']] = $v;
        }
        foreach ($query as $key=>$val)
        {
            if(isset($result[$val['technician_id']])){
                $query[$key]['technician_name'] = $result[$val['technician_id']]['name'];
                $query[$key]['technician_mobile'] = $result[$val['technician_id']]['mobile'];
                $query[$key]['department_id'] = $result[$val['technician_id']]['store_id'];
            }
        }
        return $query;
    }
    /**
     * 技师接单统计  报表
     */
    public static function technicianOrder()
    {
        $star = strtotime(date("Y-m-1 00:00:00",strtotime('-1 month')));
        $end = strtotime(date("Y-m-t 23:59:59",strtotime('-1 month')));
        $where = [
            'a.is_self' => [1,3],
            'a.type'    => 1,
        ];
        //总接单数
        $query = self::find()
            ->from(self::tableName() . ' as a ')
            ->leftJoin(['`'.Work::tableName().'` as c'] , 'a.work_no = c.work_no')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'c.order_no = b.order_no')
            ->select('a.technician_id,count(a.work_no) as work_num,FROM_UNIXTIME(a.create_time,"%Y-%m-%d") as `days`,b.work_type')
            ->where($where)
            ->andwhere('c.cancel_status = 1')
            ->andWhere("a.create_time >=".$star." and a.create_time <=".$end)
            ->groupBy('a.technician_id,days,b.work_type')
            ->asArray()->all();
       if($query)
       {
           $transaction = Yii::$app->db->beginTransaction();
           try {
                //查询技师名字及所属机构
               $result = self::getName($query);
               $key = ['technician_id','technician_name','technician_mobile','department_id','direct_company_id','work_num','work_type','date'];
               foreach ($result as $k=>$v){
                   $value[$k][] = $v['technician_id'];
                   $value[$k][] = $v['technician_name'];
                   $value[$k][] = $v['technician_mobile'];
                   $value[$k][] = $v['department_id'];
                   $value[$k][] = Department::getDirectCompanyId($v['department_id']);
                   $value[$k][] = $v['work_num'];
                   $value[$k][] = $v['work_type'];
                   $value[$k][] = $v['days'];
               }
               $res = ReportingTechnicianOrder::addBatch($key,$value);
               //查询已完成订单数量
               $finish = self::find()
                   ->from(self::tableName() . ' as a ')
                   ->leftJoin(['`'.Work::tableName().'` as c'] , 'a.work_no = c.work_no')
                   ->leftJoin(['`'.WorkOrder::tableName().'` as o'] , 'c.order_no = o.order_no')
                   ->select('a.technician_id,o.work_type,count(a.work_no) as finish_num,FROM_UNIXTIME(c.update_time,"%Y-%m-%d") as `date`')
                   ->where($where)
                   ->andWhere("c.status = 4 and c.update_time >=".$star." and c.update_time <=".$end)
                   ->groupBy('a.technician_id,date,o.work_type')
                   ->asArray()->all();
               if(!empty($finish))
               {
                   foreach ($finish as $k=>$v)
                   {
                       $res = ReportingTechnicianOrder::findOne(['technician_id'=>$v['technician_id'],'work_type'=>$v['work_type'],'date'=>$v['date']]);
                       if($res) {
                           ReportingTechnicianOrder::updateAll(['finish_num'=>$v['finish_num']],['id'=>$res->id]);
                       }else{
                           $t = Technician::getTechnicianInfo($v['technician_id']);
                           $v['technician_name'] = $t['name'];
                           $v['technician_mobile'] = $t['mobile'];
                           $v['department_id']   = $t['store_id'];
                           $v['direct_company_id']   = Department::getDirectCompanyId($t['store_id']);
                           ReportingTechnicianOrder::add($v);
                       }
                   }
               }
               //技师全部已评
               $app = self::find()
                   ->from(self::tableName() . ' as a ')
                   ->leftJoin(['`'.WorkAppraisal::tableName().'` as c'] , 'a.work_no = c.work_no')
                   ->leftJoin(['`'.WorkOrder::tableName().'` as o'] , 'c.order_no = o.order_no')
                   ->select('a.technician_id,FROM_UNIXTIME(c.work_finish_time,"%Y-%m-%d") as `date`,count(c.id) as all_appraise_num,o.work_type')
                   ->where($where)
                   ->andWhere("c.appraisal_status = 2 and c.work_finish_time >=".$star." and c.work_finish_time <=".$end)
                   ->groupBy('a.technician_id,date,o.work_type')
                   ->asArray()->all();
               if(!empty($app))
               {
                   foreach ($app as $k=>$v)
                   {
                       $res = ReportingTechnicianOrder::findOne(['technician_id'=>$v['technician_id'],'work_type'=>$v['work_type'],'date'=>$v['date']]);
                       if($res) {
                           ReportingTechnicianOrder::updateAll(['all_appraise_num'=>$v['all_appraise_num']],['id'=>$res->id]);
                       }else{
                           $t = Technician::getTechnicianInfo($v['technician_id']);
                           $v['technician_name'] = $t['name'];
                           $v['technician_mobile'] = $t['mobile'];
                           $v['department_id']   = $t['store_id'];
                           $v['direct_company_id']   = Department::getDirectCompanyId($t['store_id']);
                           ReportingTechnicianOrder::add($v);
                       }
                   }
               }
               //好评
               $good_app = self::find()
                   ->from(self::tableName() . ' as a ')
                   ->leftJoin(['`'.WorkAppraisal::tableName().'` as c'] , 'a.work_no = c.work_no')
                   ->leftJoin(['`'.WorkOrder::tableName().'` as o'] , 'c.order_no = o.order_no')
                   ->select('a.technician_id,FROM_UNIXTIME(c.work_finish_time,"%Y-%m-%d") as `date`,count(c.id) as goods_appraise_num,o.work_type')
                   ->where($where)
                   ->andWhere("c.appraisal_status=2 and star=1 and c.work_finish_time >=".$star." and c.work_finish_time <=".$end)
                   ->groupBy('a.technician_id,date,o.work_type')
                   ->asArray()->all();
               if(!empty($good_app))
               {
                   foreach ($good_app as $k=>$v)
                   {
                       $res = ReportingTechnicianOrder::findOne(['technician_id'=>$v['technician_id'],'work_type'=>$v['work_type'],'date'=>$v['date']]);
                       if($res) {
                           ReportingTechnicianOrder::updateAll(['goods_appraise_num'=>$v['goods_appraise_num']],['id'=>$res->id]);
                       }else{
                           $t = Technician::getTechnicianInfo($v['technician_id']);
                           $v['technician_name'] = $t['name'];
                           $v['technician_mobile'] = $t['mobile'];
                           $v['department_id']   = $t['store_id'];
                           $v['direct_company_id']   = Department::getDirectCompanyId($t['store_id']);
                           ReportingTechnicianOrder::add($v);
                       }
                   }
               }
               //差评
               $bad_app = self::find()
                   ->from(self::tableName() . ' as a ')
                   ->leftJoin(['`'.WorkAppraisal::tableName().'` as c'] , 'a.work_no = c.work_no')
                   ->leftJoin(['`'.WorkOrder::tableName().'` as o'] , 'c.order_no = o.order_no')
                   ->select('a.technician_id,FROM_UNIXTIME(c.work_finish_time,"%Y-%m-%d") as `date`,count(c.id) as bad_appraise_num,o.work_type')
                   ->where($where)
                   ->andWhere("c.appraisal_status=2 and star=3 and c.work_finish_time >=".$star." and c.work_finish_time <=".$end)
                   ->groupBy('a.technician_id,date,o.work_type')
                   ->asArray()->all();
               if(!empty($bad_app))
               {
                   foreach ($bad_app as $k=>$v)
                   {
                       $res = ReportingTechnicianOrder::findOne(['technician_id'=>$v['technician_id'],'work_type'=>$v['work_type'],'date'=>$v['date']]);
                       if($res) {
                           ReportingTechnicianOrder::updateAll(['bad_appraise_num'=>$v['bad_appraise_num']],['id'=>$res->id]);
                       }else{
                           $t = Technician::getTechnicianInfo($v['technician_id']);
                           $v['technician_name'] = $t['name'];
                           $v['technician_mobile'] = $t['mobile'];
                           $v['department_id']   = $t['store_id'];
                           $v['direct_company_id']   = Department::getDirectCompanyId($t['store_id']);
                           ReportingTechnicianOrder::add($v);
                       }
                   }
               }

               //按时上门工单数量
               $ontime = self::find()
                   ->from(self::tableName() . ' as a ')
                   ->leftJoin(['`'.Work::tableName().'` as c'] , 'a.work_no = c.work_no')
                   ->leftJoin(['`'.WorkProcess::tableName().'` as p'] , 'a.work_no = p.work_no')
                   ->leftJoin(['`'.WorkOrder::tableName().'` as o'] , 'c.order_no = o.order_no')
                   ->select('a.technician_id,FROM_UNIXTIME(p.create_time,"%Y-%m-%d") as `date`,count(p.work_no) as on_time,o.work_type')
                   ->where($where)
                   ->andWhere("p.create_time >=".$star." and p.create_time <=".$end." and p.type = 4 and p.create_time <= c.plan_time")
                   ->groupBy('a.technician_id,date,o.work_type')
                   ->asArray()->all();
               if(!empty($ontime))
               {
                   foreach ($ontime as $k=>$v)
                   {
                       $res = ReportingTechnicianOrder::findOne(['technician_id'=>$v['technician_id'],'work_type'=>$v['work_type'],'date'=>$v['date']]);
                       if($res) {
                           ReportingTechnicianOrder::updateAll(['on_time'=>$v['on_time']],['id'=>$res->id]);
                       }else{
                           $t = Technician::getTechnicianInfo($v['technician_id']);
                           $v['technician_name'] = $t['name'];
                           $v['technician_mobile'] = $t['mobile'];
                           $v['department_id']   = $t['store_id'];
                           $v['direct_company_id']   = Department::getDirectCompanyId($t['store_id']);
                           ReportingTechnicianOrder::add($v);
                       }
                   }
               }
               $transaction->commit();
           } catch (\Exception $e)
           {
               $transaction->rollBack();
           }
       }

    }
}



