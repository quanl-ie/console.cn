<?php
namespace console\models;

use common\helpers\Helper;
use common\models\Common;
use common\models\Order;
use common\models\WorkRelTechnician;
use Yii;

class Message extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->service_provider_db;
    }

    public static function tableName()
    {
        return 'message';
    }
    
    /**
     * 获取当前还没有发送的消息
     */
    public static function getMessageAll() {
        $message = self::find()->where(['status' => 0])->asArray()->all();
        return $message;
    }

    /**
     * 发送消息
     * @param $type
     * @param $orderNo
     * @param $workNo
     * @param $technicianId
     * @param $technicianName
     * @return array
     */
    public static function send($type,$orderNo,$workNo,$technicianId,$technicianName)
    {
        $orderData = Order::findOneByOrderNo($orderNo);
        if($orderData)
        {
            $srcType = $orderData['src_type'];
            $srcId   = $orderData['src_id'];
            $serviceProvideId = $orderData['service_provide_id'];

            $className      = Common::getClassNameBySaleId($orderData['sale_order_id']);
            $workName       = Common::getWorktypeName($orderData['work_type']);
            $storeName      = Common::getStoreName($orderData['service_provide_id']);

            $content = [
                'plan_time'  => date("Y-m-d H:i:s",$orderData['plan_time']),
                'order_no'   => $orderNo,
                'class_name' => $className,
                'work_name'  => $workName,
                'status'     => $orderData['status'],
                'work_no'    => $workNo
            ];
            $content = json_encode($content);

            //服务商指派技师
            if($type == 8)
            {
                $title = "【已指派技师】".$storeName."指派技师".$technicianName."为客户服务";
                $srcUserIds = Common::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $serviceProvideId, $val);
                    }
                }
                $title = "您有新的工单！请查看！";
                foreach ($technicianId as $val){
                    self::callSendApi(\common\models\BaseModel::SRC_JS,$val,$title,$content,$serviceProvideId,$val);
                }
            }
            //服务商改派技师
            else if($type == 9)
            {
                $title = "【改派技师】".$storeName."改派技师".$technicianName."为客户服务";
                $srcUserIds = Common::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $serviceProvideId, $val);
                    }
                }

                $title = "您有新的工单！请查看！";
                foreach ($technicianId as $val){
                    self::callSendApi(\common\models\BaseModel::SRC_JS,$val,$title,$content,$serviceProvideId,$val);
                }

                $title = "工单已改派！";
                $oldTechnicianArr = WorkRelTechnician::getOldTechnicianId($workNo);
                if($oldTechnicianArr){
                    foreach ($oldTechnicianArr as $val){
                        self::callSendApi(\common\models\BaseModel::SRC_JS,$val['technician_id'],$title,$content,$serviceProvideId,$val['technician_id']);
                        WorkRelTechnician::chagePushStatus($val['id']);
                    }
                }
            }
        }

        return [
            'order_no' =>$orderNo
        ];
    }

    /**
     * 调用 api 进行发送
     * @param $srcType
     * @param $srcId
     * @param $title
     * @param $content
     * @param $publishId
     * @param $subscribeId
     * @return array
     * @author  xi
     * @date 2018-1-9
     */
    public static function callSendApi($srcType,$srcId,$title,$content,$publishId,$subscribeId)
    {
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/send-msg";
        $postData = [
            'src_type'     => $srcType,
            'src_id'       => $srcId,
            'title'        => $title,
            'content'      => $content,
            'publish_id'   => $publishId,
            'subscribe_id' => $subscribeId,
            'msg_class'    => 1,
            'msg_type'     => 2,
            'publish_type' => 1,
            'link_info'    => '',
            'notice_type'  => 1,
            'notice_url'   => ''
        ];

        $jsonStr = Helper::curlPostJson($url,$postData);
        $result = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success']==true)
        {
            return [
                'success' => true,
                'message' => '发送成功'
            ];
        }
        else if(isset($result['success']))
        {
            return [
                'success' => false,
                'message' => $result['message']
            ];
        }
        return [
            'success' => false,
            'message' => $jsonStr
        ];
    }
    
}
