<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class Role extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'role';
    }

    /**
     * 初始化服务商，商家权限
     * @author xi
     */
    public static function initDefault()
    {
        $fwsMenuIds = Menu::getIdsBySrcType(\common\models\BaseModel::SRC_FWS);
        if($fwsMenuIds){
            $model = self::findOne(['id'=>12]);
            $model->menu_ids = implode(',',$fwsMenuIds);
            $model->save(false);
        }
    }

}
