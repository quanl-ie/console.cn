<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class Calc extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }



    public static function getGoodClass($prod_id)
    {
        $result = [];
        $sql = "select prod_name,brand_id,brand_name,class_id,class_name from sale_order where id = $prod_id";
        $prod_info = Yii::$app->db->createCommand($sql)
            ->queryOne();
        if($prod_info){
            $class_id = $prod_info['class_id'];
            if($class_id){
                $sql = "select c1.id as class_type ,c1.title as class_type_name,c2.id as class_type2,c2.title as class_type2_name  from  tp_service_class c1 left join tp_service_class c2 on c1.pid= c2.id  where c1.id=".$class_id;
            //print_r($sql);
                $class_info = Yii::$app->o2odb->createCommand($sql)
                    ->queryOne();
                if($class_info){
                    $result['prod_name'] =$prod_info['prod_name'];
                    $result['brand'] =$prod_info['brand_id'];
                    $result['brand_name'] =$prod_info['brand_name'];
    
                    if($class_info['class_type2']){
                        $result['class_type'] =$class_info['class_type2'];
                        $result['class_type2'] =$class_info['class_type'];
                        $result['class_type_name'] =$class_info['class_type2_name'];
                        $result['class_type2_name'] =$class_info['class_type_name'];
                    }else{
                        $result['class_type'] =$class_info['class_type'];
                        $result['class_type2'] =$class_info['class_type2'];
                        $result['class_type_name'] =$class_info['class_type'];
                        $result['class_type2_name'] =$class_info['class_name'];
                    }
                }
            }
            
        }

        return $result;
    }


    public static function syncData()
    {
        //self::syncMainTable();
        self::syncExtendTable();
    }

    public static function syncMainTable()
    {
        $table_name = "work_order";
        $sql = "select last_sync_time,table_name from sync_info where flag=1 and table_name='{$table_name}' ";
        $info = Yii::$app->order_db->createCommand($sql)
            ->queryOne();
        if($info){
            $last_time = $info['last_sync_time'];
            $sql = "select * from work_order where time_stamp>{$last_time}";
            $info_arr = Yii::$app->order_db->createCommand($sql)->queryAll();
            if($info_arr){
                foreach ($info_arr as $info)
                {
                    $good_id = $info['prod_id'];
                    $order_id =  $info['order_no'];
                    self::getGoodClass($good_id);
                    //删除已经存在的订单信息
                    $sql = "delete from order_info_tmp  where order_id= '".$order_id."'";
                    Yii::$app->order_db->createCommand($sql)
                        ->execute();
                    //拼装订单信息，插入到临时表
                    $sql = "insert into order_info_tmp values()";
                    Yii::$app->order_db->createCommand($sql)
                        ->execute();
                    //再次更新上次同步时间数据
                    $sql = "update sync_info set last_sync_time ='".$last_time."' where table_name='{$table_name}'";
                }
            }

        }
    }

    public static function syncExtendTable()
    {
        $table_name = "work_order";
        $sql = "select last_sync_time,table_name from sync_info where flag=1 and table_name='{$table_name}'";
        $info = Yii::$app->order_db->createCommand($sql)
            ->queryOne();
        if($info){
            $last_time = $info['last_sync_time'];
            $max_sync_time = $last_time;
            $sql = "select src_type,timestamp,cancel_status,src_id,sale_order_id as prod_id,work_type,w.status,w.create_time,w.order_no as order_no from work_order w,work_order_detail d where w.order_no=d.order_no  and w.timestamp>'".$last_time."'";
            $info_arr = Yii::$app->order_db->createCommand($sql)->queryAll();
            if($info_arr){
                foreach ($info_arr as $info)
                {
                    $good_id = $info['prod_id'];
                    $order_no = $info['order_no'];
                    if($good_id){
                        $prod_info = self::getGoodClass($good_id);
                    if($prod_info){

                        $max_sync_time = $info['timestamp'];
                        //删除已经存在的订单信息
                        $sql = "delete from order_info_tmp  where order_no= '".$order_no."'";
                        Yii::$app->order_db->createCommand($sql)
                            ->execute();
                        //拼装订单信息，插入到临时表

                        $command = Yii::$app->order_db->createCommand();
                        $result = $command->insert('order_info_tmp', [
                            'order_no' => $info['order_no'],
                            'create_time' =>date('Y-m-d H:i:s',$info['create_time']),
                            'status' =>$info['status'],
                            'prod_id' =>$info['prod_id'],
                            'prod_name' =>$prod_info['prod_name'],
                            'class_type' =>$prod_info['class_type'],
                            'class_type2' =>$prod_info['class_type2'],
                            'brand' =>$prod_info['brand'],
                            'service_type' =>$info['work_type'],
                            'reason' =>$info['cancel_status'],
                            'class_type_name' =>$prod_info['class_type_name'],
                            'class_type2_name' =>$prod_info['class_type2_name'],
                            'brand_name' =>$prod_info['brand_name'],
                            'create_date' =>date('Y-m-d',$info['create_time']),
                            'src_type' => $info['src_type'],
                            'src_id' => $info['src_id']
                        ])->execute();

                        $sql = $command->sql;
                    }
                    }
                    

                }
                echo $max_sync_time;
                //再次更新上次同步时间数据
                $sql = "update sync_info set last_sync_time ='".$max_sync_time."' where table_name='{$table_name}'";
                Yii::$app->order_db->createCommand($sql)
                    ->execute();
            }

        }
    }

}