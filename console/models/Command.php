<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/11/15
 * Time: 15:37
 */
namespace console\models;
use Yii;

class Command extends BaseModel
{
    public static function getDb ()
    {
        return Yii::$app->order_db;
    }

    public static function tableName ()
    {
        return 'command_center_statistics';
    }

    public static function initData()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try{
            //清空
            self::getDb()->createCommand("truncate command_center_statistics")->execute();
            //订单总数
            $order = WorkOrder::find()
                ->select("account_id,count(id) as order_total_num,FROM_UNIXTIME(create_time,'%Y-%m-%d') as `date`")
                ->groupBy('account_id,date')
                ->asArray()->all();
            $value = [];
            foreach ($order as $k=>$val)
            {
                $value[$k][] = $val['account_id'];
                $value[$k][] = $val['order_total_num'];
                $value[$k][] = $val['date'];
            }
            Yii::$app->order_db->createCommand()->batchInsert(self::tableName(),['account_id','order_total_num','date'],$value)->execute();
            //已完成订单
            $order_f = WorkOrder::find()
                ->from(WorkOrder::tableName().' as a')
                ->leftJoin(['`'.WorkOrderProcess::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("a.account_id,count(a.id) as order_finish_num,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`")
                ->where(['b.status'=>5])
                ->groupBy('a.account_id,date')
                ->asArray()->all();
            if($order_f)
            {
                foreach ($order_f as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['order_finish_num'=>$value['order_finish_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->order_finish_num = $value['order_finish_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //已取消订单
            $order_c = WorkOrder::find()
                ->from(WorkOrder::tableName().' as a')
                ->leftJoin(['`'.WorkOrderProcess::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("a.account_id,count(a.id) as order_cancel_num,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`")
                ->where(['b.status'=>6])
                ->groupBy('a.account_id,date')
                ->asArray()->all();
            if($order_c)
            {
                foreach ($order_c as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['order_cancel_num'=>$value['order_cancel_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->order_cancel_num = $value['order_cancel_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //工单总数
            $work = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_total_num")
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {

                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_total_num'=>$value['work_total_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_total_num = $value['work_total_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //工单完成总数
            $work = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_finish_num")
                ->where('a.status = 4')
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {

                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_finish_num'=>$value['work_finish_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_finish_num = $value['work_finish_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //工单取消总数
            $work = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_cancel_num")
                ->where('a.cancel_status = 2')
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_cancel_num'=>$value['work_cancel_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_cancel_num = $value['work_cancel_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //工单服务中
            $work = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_in_server_num")
                ->where('a.status = 3')
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_in_server_num'=>$value['work_in_server_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_in_server_num = $value['work_in_server_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //待评价
            $work = Work::find()
                ->from(Work::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_wait_appraise_num")
                ->where('a.status = 4 and a.appraisal_status = 1')
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_wait_appraise_num'=>$value['work_wait_appraise_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_wait_appraise_num = $value['work_wait_appraise_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //异常工单
            $work = WorkOrderProcess::find()
                ->from(WorkOrderProcess::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_abnormal_num")
                ->where('a.status = 9')
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_abnormal_num'=>$value['work_abnormal_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_abnormal_num = $value['work_abnormal_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            //已处理异常工单
            $work = WorkOrderProcess::find()
                ->from(WorkOrderProcess::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_finish_abnormal_num")
                ->where('a.status =9 and a.current = 0')
                ->groupBy("b.account_id,date")
                ->asArray()->all();
            if($work)
            {
                foreach ($work as $key=>$value)
                {
                    $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                    if($result){
                        self::updateAll(['work_finish_abnormal_num'=>$value['work_finish_abnormal_num']],['id'=>$result->id]);
                    }else{
                        $model = new self();
                        $model->account_id = $value['account_id'];
                        $model->work_finish_abnormal_num = $value['work_finish_abnormal_num'];
                        $model->date    = $value['date'];
                        $model->save();
                    }
                }
            }
            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
        }
    }
    public static function repair()
    {
        $stime = strtotime(date("Y-m-d 00:00:00"));
        $etime = strtotime(date("Y-m-d 23:30:00"));
        //订单总数
        $order = WorkOrder::find()
            ->select("account_id,count(id) as order_total_num,FROM_UNIXTIME(create_time,'%Y-%m-%d') as `date`")
            ->groupBy('account_id,date')
            ->where('create_time >= '.$stime." and create_time <=".$etime)
            ->asArray()->all();
        foreach ($order as $key=>$value)
        {
            $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
            if($result){
                self::updateAll(['order_total_num'=>$value['order_finish_num']],['id'=>$result->id]);
            }
        }
        //已完成订单
        $order_f = WorkOrder::find()
            ->from(WorkOrder::tableName().' as a')
            ->leftJoin(['`'.WorkOrderProcess::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("a.account_id,count(a.id) as order_finish_num,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`")
            ->where(['b.status'=>5])
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy('a.account_id,date')
            ->asArray()->all();
        if($order_f)
        {
            foreach ($order_f as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['order_finish_num'=>$value['order_finish_num']],['id'=>$result->id]);
                }
            }
        }
        //已取消订单
        $order_c = WorkOrder::find()
            ->from(WorkOrder::tableName().' as a')
            ->leftJoin(['`'.WorkOrderProcess::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("a.account_id,count(a.id) as order_cancel_num,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`")
            ->where(['b.status'=>6])
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy('a.account_id,date')
            ->asArray()->all();
        if($order_c)
        {
            foreach ($order_c as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['order_cancel_num'=>$value['order_cancel_num']],['id'=>$result->id]);
                }
            }
        }
        //工单总数
        $work = Work::find()
            ->from(Work::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_total_num")
            ->where('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {

                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_total_num'=>$value['work_total_num']],['id'=>$result->id]);
                }
            }
        }
        //工单完成总数
        $work = Work::find()
            ->from(Work::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_finish_num")
            ->where('a.status = 4')
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {

                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_finish_num'=>$value['work_finish_num']],['id'=>$result->id]);
                }
            }
        }
        //工单取消总数
        $work = Work::find()
            ->from(Work::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_cancel_num")
            ->where('a.cancel_status = 2')
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_cancel_num'=>$value['work_cancel_num']],['id'=>$result->id]);
                }
            }
        }
        //工单服务中
        $work = Work::find()
            ->from(Work::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_in_server_num")
            ->where('a.status = 3')
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_in_server_num'=>$value['work_in_server_num']],['id'=>$result->id]);
                }
            }
        }
        //待评价
        $work = Work::find()
            ->from(Work::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_wait_appraise_num")
            ->where('a.status = 4 and a.appraisal_status = 1')
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_wait_appraise_num'=>$value['work_wait_appraise_num']],['id'=>$result->id]);
                }
            }
        }
        //异常工单
        $work = WorkOrderProcess::find()
            ->from(WorkOrderProcess::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_abnormal_num")
            ->where('a.status = 9')
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_abnormal_num'=>$value['work_abnormal_num']],['id'=>$result->id]);
                }
            }
        }
        //已处理异常工单
        $work = WorkOrderProcess::find()
            ->from(WorkOrderProcess::tableName() . ' as a ')
            ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->select("b.account_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as work_finish_abnormal_num")
            ->where('a.status =9 and a.current = 0')
            ->andWhere('a.create_time >= '.$stime." and a.create_time <=".$etime)
            ->groupBy("b.account_id,date")
            ->asArray()->all();
        if($work)
        {
            foreach ($work as $key=>$value)
            {
                $result = self::findOne(['account_id'=>$value['account_id'],'date'=>$value['date']]);
                if($result){
                    self::updateAll(['work_finish_abnormal_num'=>$value['work_finish_abnormal_num']],['id'=>$result->id]);
                }
            }
        }
    }
}