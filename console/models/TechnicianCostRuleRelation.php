<?php
namespace console\models;

use Yii;

class TechnicianCostRuleRelation extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_cost_rule_relation';
    }
}