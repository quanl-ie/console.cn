<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class Technician extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->db;
    }
    public static function tableName()
    {
        return 'technician';
    }
    
    /**
     * 查询距离当前时间两小时之内的订单
     */
    public static function getTechnician($id) {
        return self::find()->where(['id' => $id])->asArray()->one();
    }
    //获取技师信息
    public static function getTechnicianInfo($id) {
        if(is_array($id)){
            return self::find()->where(['id' => $id])->select('id,name,mobile,store_id')->asArray()->all();
        }
        return self::find()->where(['id' => $id])->select('id,name,mobile,store_id')->asArray()->one();
    }
}
