<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class Menu extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'menu';
    }

    /**
     * 查出服务商及商家的菜单
     * @param $srcType
     * @return array
     */
    public static function getIdsBySrcType($srcType)
    {
        $where = [
            'src_type' => $srcType,
            'status'   => 1
        ];
        $query = Menu::find()->where($where)->select('id')->asArray()->all();
        if($query){
            return array_column($query,'id');
        }
        return [];
    }
}
