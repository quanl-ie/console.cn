<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class WorkOrderStatus extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_status';
    }

}