<?php
namespace console\models;

use common\helpers\Helper;
use Yii;

class HistoryPosition extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->service_provider_db;
    }

    public static function tableName()
    {
        return 'history_position';
    }
    
    /**
     * 保存位置信息
     */
    public static function savePosition($data) {
        $data = json_decode($data[1], true);
        $model = new self();
        $model->lon           = $data['lon'];
        $model->lat           = $data['lat'];
        $model->address       = $data['address'];
        $model->tec_id        = $data['tec_id'];
        $model->time          = $data['time'];
        $model->service_time  = time();
        $res = $model->save(false);
        return true;
    }
}
