<?php
namespace console\models;

use common\helpers\Helper;
use Yii;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    /**
     * 日志记录
     * @param stirng $message
     * @author xi
     */
    public static function logs($message,$filename='console.log') {
        $file =  Yii::getAlias('@runtime') .'/logs/'.date('Ym').'/'.$filename;
        if( !file_exists(dirname($file))){
            @mkdir(dirname($file),0777,true);
        }
        @file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] ", FILE_APPEND);
        @file_put_contents($file, $message . " \n", FILE_APPEND);
    }

}