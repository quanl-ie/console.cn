<?php
namespace console\models;

use common\models\CostItem;
use Yii;
use yii\helpers\ArrayHelper;

class WorkCostDivide extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost_divide';
    }

    /**
     * 添加
     * @return bool
     */
    public static function add()
    {
        //查出已完成的工单
        $calcData = Work::getWaitCalcData();
        if($calcData)
        {
            foreach ($calcData as $val)
            {
                //查出技师与服务商合作规则
                $techCostRuleArr = TechnicianCostRule::getCostRule($val['src_id'],$val['technician_id']);
                if($techCostRuleArr)
                {
                    //收费规则，用于计算价格
                    $costRuleArr = ArrayHelper::index($techCostRuleArr,'cost_item_id');
                    //$itemIds = array_column($techCostRuleArr,'cost_item_id');
                    $itemRes = CostItem::find()
                        ->where(['src_id' => $val['src_id'], 'src_type' => $val['src_type'],'status' => 1])
                        ->select('id,cs_id,cs_name')
                        ->asArray()
                        ->all();
                    if($itemRes)
                    {
                        //规则id
                        $csIds     = array_column($itemRes,'cs_id');
                        //规则名称
                        $csNameArr = array_column($itemRes,'cs_name','cs_id');
                        //转换格式
                        $itemIdArr   = array_column($itemRes,'cs_id','cs_id');

                        //查出收费项目
                        $costData = WorkCost::getCost($val['work_no'],$csIds);

                        if($costData)
                        {
                            foreach ($costData as $v)
                            {
                                //预结算金额
                                $expectAmount = 0;
                                $divideRules  = '';

                                //计算预结算金额
                                if(isset($itemIdArr[$v['cost_id']]))
                                {
                                    $itemId = $itemIdArr[$v['cost_id']];
                                    if( isset($costRuleArr[$itemId]) )
                                    {
                                        $tempCostArr = $costRuleArr[$itemId];
                                        //定额结算
                                        if($tempCostArr['cost_type'] == 1)
                                        {
                                            $expectAmount = ($tempCostArr['fixed_amount'] * $v['cost_number']) * 10000;
                                            $divideRules  = '定额('.sprintf('%.2f',$tempCostArr['fixed_amount']).'元)';
                                        }
                                        //百分比结算
                                        else if($tempCostArr['cost_type'] == 2)
                                        {
                                            $expectAmount = (($tempCostArr['pecent']/100) * $v['cost_real_amount'] ) *10000;
                                            $divideRules = '百分比('.($tempCostArr['pecent']).'%)';
                                        }
                                        $cost_name = isset($csNameArr[$v['cost_id']])?$csNameArr[$v['cost_id']]:'';
                                        $model = new self();
                                        $model->src_type        = \common\models\BaseModel::SRC_JS;
                                        $model->src_id          = $val['technician_id'];
                                        $model->process_user_id = -1;
                                        $model->order_no        = $v['order_no'];
                                        $model->work_no         = $v['work_no'];
                                        $model->payer_id        = $v['payer_id'];
                                        $model->cost_id         = $v['cost_id'];
                                        $model->cost_number     = $v['cost_number'];
                                        $model->cost_price      = $v['cost_price'];
                                        $model->cost_amount     = $v['cost_amount'];
                                        $model->cost_real_amount= $v['cost_real_amount'];
                                        $model->comment         = '';
                                        $model->create_time     = time();
                                        $model->update_time     = time();
                                        $model->cost_name       = isset($csNameArr[$v['cost_id']])?$csNameArr[$v['cost_id']]:'';
                                        $model->expect_amount   = $expectAmount;
                                        $model->divide_amount   = $expectAmount;
                                        $model->divide_rules    = $divideRules;
                                        if($model->save()){
                                            Work::updateAll(['is_calc'=>1],['work_no'=>$v['work_no']]);
                                        }
                                        $data = [];
                                        $data = [
                                            'order_no'          => $v['order_no'],
                                            'work_no'           => $v['work_no'],
                                            'cost_name'         => $cost_name,
                                            'divide_rules'      => $divideRules,
                                            'expect_amount'     => $expectAmount,
                                            'cost_real_amount'  => $v['cost_real_amount'],
                                            'technician_id'     => $val['technician_id'],

                                        ];
                                        $record = self::createLogs($data);  //写入计费日志
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        return false;
    }
    //写入计费日志
    public static function createLogs($data){
        if($data){
            $time = date("Y-m-d H:i:s");
            $a = '===start======现在是'.$time.'===开始计算工单【'.$data['work_no'].'】的分成======================'.PHP_EOL;
            $b = '订单号是     【'.$data['order_no'].'】'.PHP_EOL;
            $c = '技师id是     【'.$data['technician_id'].'】'.PHP_EOL;
            $d = '收费项目是   【'.$data['cost_name'].'】'.PHP_EOL;
            $e = '结算规则是   【'.$data['divide_rules'].'】'.PHP_EOL;
            $f = '预结算金额是 【'.$data['expect_amount'].'】'.PHP_EOL;
            $g = '实际金额是   【'.$data['cost_real_amount'].'】'.PHP_EOL;
            $z = '===end=====================工单【'.$data['work_no'].'】的分成计算结束====================================='.PHP_EOL;
            $data = $a.$b.$c.$d.$e.$f.$g.$z;
            $record  = @file_put_contents("/tmp/workCostDivideLogs.txt",$data,FILE_APPEND);
        }
    }
}