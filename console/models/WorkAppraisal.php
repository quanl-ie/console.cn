<?php
namespace console\models;

use Yii;

class WorkAppraisal extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_appraisal';
    }
}