<?php
namespace console\models;

use common\models\Account;
use common\models\AccountAddress;
use common\models\Department;
use Yii;

class Work extends BaseModel
{
    const SUM_ID_AS_FINISH_NUM = 'sum(id) as finish_num';

    public static function getDb ()
    {
        return Yii::$app->order_db;
    }

    public static function tableName ()
    {
        return 'work';
    }

    /**
     * 查出未计算分成的工单
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getWaitCalcData ()
    {
        $where = ['is_calc' => 0, 'status' => 4];
        $query = self::find()->where($where)->select('src_id,order_no,work_no,technician_id,src_type')->limit(1000)->asArray()->all();
        if ($query) {
            return $query;
        }
        return [];
    }

    /**
     * 获取所有工单
     * @param int $page
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllWorkData($page = 1,$limit = 1000)
    {
        $where = [
            'a.cancel_status' => 1,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a ')
            ->innerJoin(['`'.WorkRelTechnician::tableName().'` as b'] , 'a.work_no = b.work_no')
            ->where($where)
            ->select("a.work_no,status,plan_time")
            ->groupBy("a.work_no")
            ->limit($limit)
            ->offset(($page-1) * $limit)
            ->asArray()->all();

        return $query;
    }

    /**
     * 获取工单预约时间
     * @param $workNo
     * @return false|string
     */
    public static function getPlantime($workNo)
    {
        $query = self::find()
            ->where(['work_no' => $workNo])
            ->select('plan_time')
            ->asArray()->one();
        if($query)
        {
            return date('Y-m-d',$query['plan_time']);
        }
        return '';
    }
    /**
     * 平台派单量统计报表生成
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function companyOrder($limit = 200)
    {
        $star = strtotime(date("Y-m-1 00:00:00",strtotime('-1 month')));
        $end = strtotime(date("Y-m-t 23:59:59",strtotime('-1 month')));
        //工单总数
        $i = 1;
        do{
            $query = self::find()
                ->from(self::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->where('a.create_time >= '.$star.' and a.create_time <= '.$end)
                ->select("a.department_id,FROM_UNIXTIME(a.create_time,'%Y-%m-%d') as `date`,count(a.id) as new_work_num,b.work_type")
                ->groupBy("a.department_id,b.work_type,date")
                ->limit($limit)
                ->offset(($i-1) * $limit)
                ->asArray()->all();
            if(empty($query)){
                break;
            }
            foreach ($query as $val)
            {
                $val['direct_company_id'] = Department::getDirectCompanyId($val['department_id']);
                ReportingCompanyOrder::add($val);
            }
        }while($i++);
        //完成数
        $j = 1;
        do{
            $query = self::find()
                ->from(self::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->leftJoin(['`'.WorkProcess::tableName().'` as c'] , 'a.work_no = c.work_no')
                ->where('c.update_time >= '.$star.' and c.update_time <= '.$end)
                ->andWhere(['c.type'=>5])
                ->select("FROM_UNIXTIME(c.update_time,'%Y-%m-%d') as `days`,count(a.id) as finish_num,b.work_type,a.department_id")
                ->groupBy("a.department_id,b.work_type,days")
                ->limit($limit)
                ->offset(($j-1) * $limit)
                ->asArray()->all();
            if(empty($query)){
                break;
            }
            foreach ($query as $val)
            {
                $res = ReportingCompanyOrder::findOne(['department_id'=>$val['department_id'],'work_type'=>$val['work_type'],'date'=>$val['days']]);
                if($res) {
                    ReportingCompanyOrder::updateAll(['finish_work_num'=>$val['finish_num']],['id'=>$res->id]);
                }else{
                    $data = [];
                    $data['date'] = $val['days'];
                    $data['department_id'] = $val['department_id'];
                    $data['direct_company_id'] = Department::getDirectCompanyId($val['department_id']);
                    $data['work_type'] = $val['work_type'];
                    $data['finish_work_num'] = $val['finish_num'];
                    ReportingCompanyOrder::add($data);
                }
            }
        }while($j++);
        //取消数
        $k = 1;
        do{
            $query = self::find()
                ->from(self::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->leftJoin(['`'.WorkProcess::tableName().'` as c'] , 'a.work_no = c.work_no')
                ->where('c.update_time >= '.$star.' and c.update_time <= '.$end)
                ->andWhere(['c.type'=>10])
                ->select("FROM_UNIXTIME(c.update_time,'%Y-%m-%d') as `days`,count(a.id) as cancel_num,b.work_type,a.department_id")
                ->groupBy("a.department_id,b.work_type,days")
                ->limit($limit)
                ->offset(($k-1) * $limit)
                ->asArray()->all();
            if(empty($query)){
                break;
            }
            foreach ($query as $val)
            {
                $res = ReportingCompanyOrder::findOne(['department_id'=>$val['department_id'],'work_type'=>$val['work_type'],'date'=>$val['days']]);
                if($res) {
                    ReportingCompanyOrder::updateAll(['cancel_work_num'=>$val['cancel_num']],['id'=>$res->id]);
                }else{
                    $data = [];
                    $data['date'] = $val['days'];
                    $data['department_id'] = $val['department_id'];
                    $data['direct_company_id'] = Department::getDirectCompanyId($val['department_id']);
                    $data['work_type'] = $val['work_type'];
                    $data['cancel_work_num'] = $val['cancel_num'];
                    ReportingCompanyOrder::add($data);
                }
            }
        }while($k++);
    }
    /**
     * 客户派单统计
     */
    public static function accountOrder($limit = 500)
    {
        $star = strtotime(date("Y-m-1 00:00:00",strtotime('-1 month')));
        $end = strtotime(date("Y-m-t 23:59:59",strtotime('-1 month')));
        $i = 1;
        do{
            $query = self::find()
                ->from(self::tableName() . ' as a ')
                ->leftJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
                ->where('a.create_time >= '.$star.' and a.create_time <= '.$end." and a.is_first=1 and a.cancel_status=1")
                ->select("b.account_id,count(a.id) as sale_order_num,a.create_time as `order_create_time`,b.work_type")
                ->groupBy("b.account_id,b.work_type")
                ->limit($limit)
                ->offset(($i-1) * $limit)
                ->asArray()->all();
            if(empty($query)){
                break;
            }
            //获取客户 名称 手机号 机构 默认地址
            $accountIds = array_unique(array_column($query,'account_id'));
            $accountInfo = Account::findAllByAttributes(['id'=>$accountIds],'id,direct_company_id,mobile,account_name','id');
            $accountAddress = AccountAddress::getDefultAddressByAccountId($accountIds);
            foreach ($query as $key=>$val) {
                if (isset($accountInfo[$val['account_id']])){
                    $query[$key]['direct_company_id'] = $accountInfo[$val['account_id']]['direct_company_id'];
                    $query[$key]['account_name'] = $accountInfo[$val['account_id']]['account_name'];
                    $query[$key]['account_mobile'] = $accountInfo[$val['account_id']]['mobile'];
                }else{
                    $query[$key]['direct_company_id'] = '';
                    $query[$key]['account_name'] = '';
                    $query[$key]['account_mobile'] = '';;
                }
                if (isset($accountAddress[$val['account_id']])){
                    $query[$key]['account_area'] = $accountAddress[$val['account_id']];
                }else{
                    $query[$key]['account_area'] = '';
                }
            }
            $value = [];
            $key = ['direct_company_id','account_id','account_name','account_mobile','account_area','sale_order_num','order_create_time','work_type'];
            foreach ($query as $k=>$v){
                $value[$k][] = $v['direct_company_id'];
                $value[$k][] = $v['account_id'];
                $value[$k][] = $v['account_name'];
                $value[$k][] = $v['account_mobile'];
                $value[$k][] = $v['account_area'];
                $value[$k][] = $v['sale_order_num'];
                $value[$k][] = $v['order_create_time'];
                $value[$k][] = $v['work_type'];
            }
            ReportingAccountOrder::addBatch($key,$value);
        }while($i++);
    }
}



